#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include <string.h>

/*
 * Taller 2:
 *
 * Leer un archivo de texto, contar cuantas palabras tienen la letra e.
 *
 * */


int main(){
    int *numero_palabras;
    numero_palabras=NULL;
    numero_palabras = (int*)malloc(sizeof(int));
    *numero_palabras = 0;
    FILE *archivo = fopen("palabras.txt","r");
    char *cad;
    cad = NULL;
    cad = (char*)malloc(sizeof(char)*200);
    char *palabra_auxiliar;
    palabra_auxiliar = NULL;
    palabra_auxiliar = (char*)malloc(sizeof(char));

    while(fgets(cad,200,archivo) != NULL){

        palabra_auxiliar = strtok (cad, " ");
        while (palabra_auxiliar != NULL){
            int largo= strlen(palabra_auxiliar);
            for(int i=0;i< largo; i++){
                if(palabra_auxiliar[i] == 'e'){
                    *numero_palabras = *numero_palabras +1;
                    break;
                }else{
                    if(palabra_auxiliar[i] == 'E'){
                    *numero_palabras = *numero_palabras +1;
                    break;
                }
                }
            }
            palabra_auxiliar = strtok(NULL," ");
        }
    }
    fclose(archivo);

    free(cad);
    free(palabra_auxiliar);

    printf("El numero de palabras con e son: %d", *numero_palabras);
    return  0;
}
