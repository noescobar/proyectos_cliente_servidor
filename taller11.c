#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include <unistd.h>
#include <pthread.h>


/*
 * Taller 10:
 *
 * Modificar el taller 10 para que los dos hilos reciban los siguientes parametros:
 *
 * hilo 1: n (tablas hasta n).
 * hilo 2: m (Los primeros m numeros no primos).
 *
 * */


void *crear_proceso(void (*funcion)()){
    int *proceso_hijo;
    proceso_hijo = (int*) malloc(sizeof(int));
    *proceso_hijo = fork();
    switch (*proceso_hijo){
            case -1:
                printf("\n Ha sucedido un error y no puede iniciarse el hilo");
                break;
            case 0:
                funcion();
            break;
            default:
                break;
        }
    return NULL;
}

void *hacermultiplicaciones(int *numero_limite, int *frecuencia){
    int *numero_auxiliar1;
    numero_auxiliar1 = (int*)malloc(sizeof(int));
    *numero_auxiliar1 = 1;
    int *numero_auxiliar2;
    numero_auxiliar2 = (int*)malloc(sizeof(int));
    *numero_auxiliar2 = 1;

    while(*numero_auxiliar1 <= *numero_limite) {
        *numero_auxiliar2 = 1;
        while(*numero_auxiliar2 <= 10) {
            sleep((unsigned)*frecuencia);
            printf("\n %d * %d = %d", *numero_auxiliar1, *numero_auxiliar2, *numero_auxiliar1 * *numero_auxiliar2);
            *numero_auxiliar2 = *numero_auxiliar2 + 1;
        }
        *numero_auxiliar1 = *numero_auxiliar1 + 1;
    }

    free(numero_auxiliar1);
    free(numero_auxiliar2);

    return NULL;
}

int *es_primo_i(int *numero){
    int *raiz = (int*) malloc(sizeof(int)*1);
    int *bool= (int*) malloc(sizeof(int)*1);
    int *j= (int*) malloc(sizeof(int)*1);
    *raiz =(int)sqrt((double)*numero);
    *bool = 1;
    for (*j = 2; *j <= *raiz; *j = *j + 1) {
        if(*numero % *j == 0){
            *bool = 0;
            break;
        }
    }
    free(raiz);
    free(j);
    return bool;
}



void *numeros_no_primos(int *numero_limite, int *frecuencia){

    int *i;
    i = (int*) malloc(sizeof(int)*1);
    int *raiz;
    raiz = (int*) malloc(sizeof(int)*1);
    int *es_primo;
    es_primo = (int*) malloc(sizeof(int)*1);
    *es_primo = 1;
    for(*i=1; *i<= *numero_limite; *i = *i + 1){
        es_primo = es_primo_i(i);
        if(*es_primo == 0){
            sleep((unsigned)*frecuencia);
            printf("\n Un numero no primo es : %i \n", *i);
        }
    }
    free(i);
    free(es_primo);
    free(raiz);

    return NULL;
}


void*hilo_1(void* unused){
    int *multiplicaciones;
    multiplicaciones = (int*)unused;
    int *frecuencia;
    frecuencia = (int*)malloc(sizeof(int));
    *frecuencia = 1;
    hacermultiplicaciones(multiplicaciones, frecuencia);
    free(multiplicaciones);
    free(frecuencia);
    return NULL;
}

void*hilo_2(void* unused){
    int *limite_primo;
    limite_primo = (int*)unused;
    int *frecuencia;
    frecuencia = (int*)malloc(sizeof(int));
    *frecuencia = 1;
    numeros_no_primos(limite_primo, frecuencia);
    free(limite_primo);
    free(frecuencia);
    return NULL;
}


int main() {
    pthread_t thread_id_1;
    pthread_t thread_id_2;
    int *limite_multiplicacion = (int*) malloc(sizeof(int)*1);
    int *limite_primos = (int*) malloc(sizeof(int)*1);
    printf("\n Ingrese el numero N limite de las multiplicaciones");
    scanf("%d",limite_multiplicacion);
    printf("\n Ingrese el numero M limite de los no primos");
    scanf("%d",limite_primos);
    //scanf("%d", );
    pthread_create(&thread_id_1, NULL, &hilo_1, limite_multiplicacion);
    pthread_create(&thread_id_2, NULL, &hilo_2, limite_primos);
    pthread_join (thread_id_1, NULL);
    pthread_join (thread_id_2, NULL);
    printf("\n");
    return  0;
}
