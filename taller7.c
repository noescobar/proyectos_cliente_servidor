#include <stdio.h>
// #include <math.h>
#include <malloc.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <string.h>
//#include <sys/types.h>
#include <sys/wait.h>
/*
 * Taller 7:
 *
 * modificar el taller 5, para que manipulando senales imprima el codigo de salida de los dos procesos.
 *
 * */

sig_atomic_t child_exit_status;

void clean_up_child_process (int signal_number){
/* Clean up the child process.  */
    int status;
    status = 0;
    wait(&status);
    printf("\n proceso terminado con codigo %d \n", status);
}

int *crear_proceso(void (*funcion)()){
    int *proceso_hijo;
    proceso_hijo = (int*) malloc(sizeof(int));
    *proceso_hijo = fork();
    switch (*proceso_hijo){
            case -1:
                printf("\n Ha sucedido un error y no puede iniciarse el hilo");
                break;
            case 0:
                funcion();
            break;
            default:
                break;
        }
    return proceso_hijo;
}

void proceso_1(){
    char* arg_list[] = {
            "./taller7multiplicacion",
            NULL
    };
    execvp("./taller7multiplicacion", arg_list);
    printf("Un error a ocurrido al ejecutar el proceso multiplicacion");
    //free(arg_list);
}

void proceso_2(){
    char* arg_list[] = {
            "./taller7primos",
            NULL
    };
    execvp("./taller7primos", arg_list);
    printf("Un error a ocurrido al ejecutar el proceso primos");
    //free(arg_list);
}


int main() {
    /* Handle SIGCHLD by calling clean_up_child_process.  */
    struct sigaction sigchld_action;
    memset (&sigchld_action, 0, sizeof (sigchld_action));
    sigchld_action.sa_handler = &clean_up_child_process;
    sigaction (SIGCHLD, &sigchld_action, NULL);

    int *procesos_hijos;
    procesos_hijos = (int*) malloc(sizeof(int)*2);
    sleep(1);
    procesos_hijos[1] = *crear_proceso(&proceso_2);
    sleep(1);
    procesos_hijos[0] = *crear_proceso(&proceso_1);
    if(procesos_hijos[0] != 0) {
        procesos_hijos[1] = *crear_proceso(&proceso_2);
        if(procesos_hijos[1] != 0) {
           sleep(16);
        }
    }
    return  0;


    return  3;
}
