#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>        


#define PORT 3550


#define MAXDATASIZE 100

void cleanup(buf)
        char *buf;
{
    int i;
    for(i=0; i<MAXDATASIZE; i++) buf[i]='\0';
}


int main(int argc, char *argv[])
{
    int fd, numbytes;
    /* ficheros descriptores */

    char buf[MAXDATASIZE];
    /* en donde es almacenará el texto recibido */

    struct hostent *he;
    /* estructura que recibirá información sobre el nodo remoto */

    struct sockaddr_in server;
    /* información sobre la dirección del servidor */

    if (argc !=2) {
        /* esto es porque nuestro programa sólo necesitará un
        argumento, (la IP) */
        printf("\n Porfavor siga las intrucciones de uso de programa para conectarse al servidor.");
        printf("\nUso: %s <Dirección IP>\n",argv[0]);
        exit(-1);
    }

    if ((he=gethostbyname(argv[1]))==NULL){
        /* llamada a gethostbyname() */
        printf("gethostbyname() error\n");
        exit(-1);
    }

    if ((fd=socket(AF_INET, SOCK_STREAM, 0))==-1){
        /* llamada a socket() */
        printf("socket() error\n");
        exit(-1);
    }

    server.sin_family = AF_INET;
    server.sin_port = htons(PORT);
    /* htons() es necesaria nuevamente ;-o */
    server.sin_addr = *((struct in_addr *)he->h_addr);
    /*he->h_addr pasa la información de ``*he'' a "h_addr" */
    bzero(&(server.sin_zero),8);

    if(connect(fd, (struct sockaddr *)&server,
               sizeof(struct sockaddr))==-1){
        /* llamada a connect() */
        printf("connect() error\n");
        exit(-1);
    }

    if ((numbytes=(int)recv(fd,buf,MAXDATASIZE,0)) == -1){
        /* llamada a recv() */
        printf("Error en recv() \n");
        exit(-1);
    }

    buf[numbytes]='\0';

    printf("Mensaje del Servidor: %s\n",buf);
    /* muestra el mensaje de bienvenida del servidor =) */

    cleanup(buf);

    char *opcion_elegida_char = (char *) malloc(sizeof(char)*40);
    int *opcion_elegida_int = (int *) malloc(sizeof(int)*1);
    int *numero = (int *) malloc(sizeof(int)*1);
    *opcion_elegida_int = 0;
    while(*opcion_elegida_int != 3){
        printf("\n1. Hallar el ultimo numero primo de un numero");
        printf("\n2. Hallar el fibonacci de un numero.");
        printf("\n3. Terminar programa y cerrar conexion.");
        printf("\n");
        scanf("%d", opcion_elegida_int);
        switch(*opcion_elegida_int){
            case 1:
                printf("\nDigite el numero al cual se realizara la operacion :");
                scanf("%d",numero);
                sprintf(opcion_elegida_char, "primo %d",*numero);
                break;
            case 2:
                printf("\nDigite el numero al cual se realizara la operacion :");
                scanf("%d",numero);
                sprintf(opcion_elegida_char, "fibonacci %d",*numero);
                break;
            case 3:
                break;
            default:
                printf("\n Opcion no valida.\n");
                break;
        }
        send(fd,opcion_elegida_char,22,0);
        cleanup(buf);
        printf("\nSe ha enviado la solicitud al servidor.\n");
        if ((numbytes=(int)recv(fd,buf,22,0)) == -1){
            /* llamada a recv() */
            printf("Error en recv() \n");
            exit(-1);
        }
        buf[numbytes]='\0';
        printf("\n Se ha recibido un respuesta del servidor : %s \n", buf);

    }

    close(fd);

    return  0;

}

