#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include <unistd.h>


/*
 * Taller 5:
 *
 *  PS3: proceso primos.
 *
 * */




void *numeros_primos(int *numero_limite, int *frecuencia){
    int *i;
    i = (int*) malloc(sizeof(int)*1);
    int *j;
    j = (int*) malloc(sizeof(int)*1);
    int *raiz;
    raiz = (int*) malloc(sizeof(int)*1);
    int *es_primo;
    es_primo = (int*) malloc(sizeof(int)*1);
    *es_primo = 1;
    for(*i=0; *i<= *numero_limite; *i = *i + 1){
        *raiz =(int)sqrt((double)*numero_limite);
        *es_primo = 1;
        for (*j = 2; *j < *raiz; *j = *j + 1) {
            if(*i % *j == 0){
                *es_primo = 0;
                break;
            }
        }

        if(*es_primo == 1){
            sleep((unsigned)*frecuencia);
            printf("\n Un numero primo es : %d", *i);
        }
    }
    free(i);
    free(j);
    free(es_primo);
    free(raiz);

    return NULL;
}



int main() {
    int *numeros;
    numeros = (int*)malloc(sizeof(int));
    *numeros = 20;
    int *frecuencia;
    frecuencia = (int*)malloc(sizeof(int));
    *frecuencia = 1;
    numeros_primos(numeros, frecuencia);
    free(numeros);
    free(frecuencia);

    return  1;
}
