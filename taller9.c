#include <stdio.h>
// #include <math.h>
#include <malloc.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
//#include <sys/types.h>
#include <math.h>
#include <sys/wait.h>


/*
 * Taller 9:
 *
 * Crea un programa que manipule la senal sigterm y solamente se elimina on una segunda senal enviada.
 * */


int *crear_proceso(void (*funcion)()){
    int *proceso_hijo;
    proceso_hijo = (int*) malloc(sizeof(int));
    *proceso_hijo = fork();
    switch (*proceso_hijo){
            case -1:
                printf("\n Ha sucedido un error y no puede iniciarse el hilo");
                break;
            case 0:
                funcion();
            break;
            default:
                break;
        }
    return proceso_hijo;
}

sig_atomic_t intentos = 1;

void clean_up_child_process (int signal_number){
/* Clean up the child process.  */
    //int status;
    //status = 0;
    if(intentos == 2) {
        printf("\n Terminando proceso hijo. \n");
        exit(1);
    } else{
        printf("\n Primer intento de terminanr el proceso \n");
        intentos ++;
    }
}

void *hacermultiplicaciones(int *numero_limite, int *frecuencia){
    int *numero_auxiliar1;
    numero_auxiliar1 = (int*)malloc(sizeof(int));
    *numero_auxiliar1 = 1;
    int *numero_auxiliar2;
    numero_auxiliar2 = (int*)malloc(sizeof(int));
    *numero_auxiliar2 = 1;

    while(*numero_auxiliar1 <= *numero_limite) {
        *numero_auxiliar2 = 1;
        while(*numero_auxiliar2 <= 10) {
            sleep((unsigned)*frecuencia);
            printf("\n %d * %d = %d", *numero_auxiliar1, *numero_auxiliar2, *numero_auxiliar1 * *numero_auxiliar2);
            *numero_auxiliar2 = *numero_auxiliar2 + 1;
        }
        *numero_auxiliar1 = *numero_auxiliar1 + 1;
    }

    free(numero_auxiliar1);
    free(numero_auxiliar2);

    return NULL;
}


void *numeros_primos(int *numero_limite, int *frecuencia){
    int *i;
    i = (int*) malloc(sizeof(int)*1);
    int *j;
    j = (int*) malloc(sizeof(int)*1);
    int *raiz;
    raiz = (int*) malloc(sizeof(int)*1);
    int *es_primo;
    es_primo = (int*) malloc(sizeof(int)*1);
    *es_primo = 1;
    for(*i=0; *i<= *numero_limite; *i = *i + 1){
        *raiz =(int)sqrt((double)*numero_limite);
        *es_primo = 1;
        for (*j = 2; *j < *raiz; *j = *j + 1) {
            if(*i % *j == 0){
                *es_primo = 0;
                break;
            }
        }
        if(*es_primo == 1){
            sleep((unsigned)*frecuencia);
            printf("\n Un numero primo es : %d", *i);
        }
    }
    free(i);
    free(j);
    free(es_primo);
    free(raiz);

    return NULL;
}


void proceso_1(){
    int *multiplicaciones;
    multiplicaciones = (int*)malloc(sizeof(int));
    *multiplicaciones = 100;
    int *frecuencia;
    frecuencia = (int*)malloc(sizeof(int));
    *frecuencia = 1;
    hacermultiplicaciones(multiplicaciones, frecuencia);
    free(multiplicaciones);
    free(frecuencia);
}

void proceso_2(){
    int *mumero_limite;
    mumero_limite = (int*)malloc(sizeof(int));
    *mumero_limite = 50;
    int *frecuencia;
    frecuencia = (int*)malloc(sizeof(int));
    *frecuencia = 2;
    numeros_primos(mumero_limite, frecuencia);
    free(mumero_limite);
    free(frecuencia);
}


int main() {
    /* Handle SIGCHLD by calling clean_up_child_process.  */
    struct sigaction sigchld_action;
    int status;
    memset (&sigchld_action, 0, sizeof (sigchld_action));
    sigchld_action.sa_handler = &clean_up_child_process;
    sigaction (SIGTERM, &sigchld_action, NULL);
    int *procesos_hijos;
    procesos_hijos = (int*) malloc(sizeof(int)*2);
    int *proceso_aleatorio;
    proceso_aleatorio = (int*) malloc(sizeof(int)*1);
    procesos_hijos[0] = *crear_proceso(&proceso_1);
    if(procesos_hijos[0] != 0) {
        procesos_hijos[1] = *crear_proceso(&proceso_2);
        if(procesos_hijos[1] != 0) {
            *proceso_aleatorio = 0;
            sleep(10);
            kill(procesos_hijos[*proceso_aleatorio], SIGTERM);
            sleep(10);
            kill(procesos_hijos[*proceso_aleatorio], SIGTERM);
            waitpid(procesos_hijos[1], &status, 0);
        }
    }
    return  0;
}
