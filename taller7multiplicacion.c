#include <stdio.h>
// #include <math.h>
#include <malloc.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
//#include <sys/types.h>
#include <sys/wait.h>


/*
 * Taller 7:
 * */

sig_atomic_t child_exit_status;

void clean_up_child_process (int signal_number){
/* Clean up the child process.  */

    printf("proceso terminado con codigo %d", signal_number);
    int *status;
    status = (int*)malloc(sizeof(int));
    *status  = 3;
    child_exit_status  = 3;
    printf("proceso terminado con codigo %d", child_exit_status);
    exit(*status);
}


void *hacer_multiplicaciones(int *numero_limite, int *frecuencia){
    int *numero_auxiliar1;
    numero_auxiliar1 = (int*)malloc(sizeof(int));
    *numero_auxiliar1 = 1;
    int *numero_auxiliar2;
    numero_auxiliar2 = (int*)malloc(sizeof(int));
    *numero_auxiliar2 = 1;

    while(*numero_auxiliar1 <= *numero_limite) {
        *numero_auxiliar2 = 1;
        while(*numero_auxiliar2 <= 10) {
            sleep((unsigned)*frecuencia);
            printf("\n %d * %d = %d", *numero_auxiliar1, *numero_auxiliar2, *numero_auxiliar1 * *numero_auxiliar2);
            *numero_auxiliar2 = *numero_auxiliar2 + 1;
        }
        *numero_auxiliar1 = *numero_auxiliar1 + 1;
    }

    free(numero_auxiliar1);
    free(numero_auxiliar2);

    return NULL;
}


int main() {

    int *multiplicaciones;
    multiplicaciones = (int*)malloc(sizeof(int));
    *multiplicaciones = 1;
    int *frecuencia;
    frecuencia = (int*)malloc(sizeof(int));
    *frecuencia = 1;
    hacer_multiplicaciones(multiplicaciones, frecuencia);
    free(multiplicaciones);
    free(frecuencia);
    return  3;
}
