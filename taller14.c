#include <stdlib.h>
#include <malloc.h>
#include <pthread.h>
#include <unistd.h>
#include <math.h>
#include <semaphore.h>

/*
 * Taller 14:
 *
 * Hacer el taller 13 con semaforos.
 *
 * */


int *es_primo_i(int *numero){
    int *raiz = (int*) malloc(sizeof(int)*1);
    int *bool= (int*) malloc(sizeof(int)*1);
    int *j= (int*) malloc(sizeof(int)*1);
    *raiz =(int)sqrt((double)*numero);
    *bool = 1;
    for (*j = 2; *j <= *raiz; *j = *j + 1) {
        if(*numero % *j == 0){
            *bool = 0;
            break;
        }
    }
    free(raiz);
    free(j);
    return bool;
}


int *ultimo_numero_primo(int *numero_limite){
    int *i= (int*) malloc(sizeof(int)*1);
    int *j = (int*) malloc(sizeof(int)*1);
    int *raiz = (int*) malloc(sizeof(int)*1);
    int *es_primo = (int*) malloc(sizeof(int)*1);
    *es_primo = 1;
    int *ultimo_primo = (int*) malloc(sizeof(int)*1);
    *ultimo_primo = 1;
    for(*i=1; *i<= *numero_limite; *i = *i + 1){
        es_primo = es_primo_i(i);
        if(*es_primo == 1){
            *ultimo_primo = *i;
        }
    }

    free(i);
    free(j);
    free(raiz);
    free(es_primo);

    return ultimo_primo;
}


int *fibonacci(int *numero){
    int *bool = (int*)malloc(sizeof(int)*1);
    int *ultimo_numero_fibonacci= (int*)malloc(sizeof(int)*1);
    *ultimo_numero_fibonacci = 1;
    int *penultimo_numero_fibonacci= (int*)malloc(sizeof(int)*1);
    *penultimo_numero_fibonacci = 0;
    int *i = (int*)malloc(sizeof(int)*1);
    *bool = 0;
    for(*i = 0; *i <=*numero & *ultimo_numero_fibonacci <= *numero; (*i)++){
        *ultimo_numero_fibonacci = *ultimo_numero_fibonacci + *penultimo_numero_fibonacci;
        *penultimo_numero_fibonacci = *ultimo_numero_fibonacci - *penultimo_numero_fibonacci;
    }
    return ultimo_numero_fibonacci;
}


struct numeros{
    struct numeros *siguiente;
    int *numero;
};

struct numeros *lista_numeros;

pthread_mutex_t mutex_lista_numero = PTHREAD_MUTEX_INITIALIZER;
sem_t *semaforo_lista_numero;


void inicializador_semaforo(){
    semaforo_lista_numero = (sem_t*)malloc(sizeof(sem_t));
    sem_init (semaforo_lista_numero, 0, 0);
}

int *generar_numero_aletorio(){
    int *numero_aletorio = (int *) malloc(sizeof(int)*1);
    *numero_aletorio = rand()%50;
    return numero_aletorio;
}


void *hilo_generador_numeros(void *args){
    struct numeros *numeros_auxiliares;
    int *running = (int*)malloc(sizeof(int)*1);
    *running = 1;
    while(*running){
        pthread_mutex_lock (&mutex_lista_numero);
        if(lista_numeros != NULL){
            if(lista_numeros->siguiente == NULL){
                numeros_auxiliares = (struct numeros*)malloc(sizeof(struct numeros));
                lista_numeros->siguiente = numeros_auxiliares;
                numeros_auxiliares->numero = generar_numero_aletorio();
            }
        }else{
            numeros_auxiliares = (struct numeros*)malloc(sizeof(struct numeros));
            lista_numeros = numeros_auxiliares;
            lista_numeros->numero = generar_numero_aletorio();
        };
        sem_post (semaforo_lista_numero);
        pthread_mutex_unlock(&mutex_lista_numero);
        sleep(1);
    }
    return NULL;
}

void *hilo_fibonacci(void* args){
    struct numeros *numeros_auxiliares;
    int *running = (int*)malloc(sizeof(int)*1);
    int *numero_auxiliar;
    numero_auxiliar = (int*)malloc(sizeof(int));
    *running = 1;
    while(*running){
        sem_wait(semaforo_lista_numero);
        pthread_mutex_lock (&mutex_lista_numero);
        if(lista_numeros != NULL){
            *numero_auxiliar = *lista_numeros->numero;
            printf("\n El fibonacci del numero %d es %d",*numero_auxiliar, *fibonacci(numero_auxiliar));
            if(lista_numeros->siguiente != NULL){
                numeros_auxiliares = lista_numeros;
                lista_numeros = lista_numeros->siguiente;
                free(numeros_auxiliares);
            }
            numeros_auxiliares = lista_numeros;
            lista_numeros = NULL;
            free(numeros_auxiliares);
        }
        pthread_mutex_unlock(&mutex_lista_numero);
        sleep(1);
    }
    return NULL;
}

void *hilo_primo(void* args){
    struct numeros *numeros_auxiliares;
    int *numero_auxiliar;
    int *result;
    numero_auxiliar = (int*)malloc(sizeof(int));
    result = (int*)malloc(sizeof(int));
    int *running = (int*)malloc(sizeof(int)*1);
    *running = 1;
    while (*running){
        sem_wait(semaforo_lista_numero);
        pthread_mutex_lock (&mutex_lista_numero);
        if(lista_numeros != NULL){
            *numero_auxiliar = *lista_numeros->numero;
            result = (int*)malloc(sizeof(int));
            result = ultimo_numero_primo(numero_auxiliar);
            printf("\n El primo del numero %d es %d", *numero_auxiliar, *result) ;
            if(lista_numeros->siguiente != NULL){
                numeros_auxiliares = lista_numeros;
                lista_numeros = lista_numeros->siguiente;
                free(numeros_auxiliares);
            }
            numeros_auxiliares = lista_numeros;
            lista_numeros = NULL;
            free(numeros_auxiliares);
            free(result);
        }
        pthread_mutex_unlock(&mutex_lista_numero);
        sleep(1);
    }
    return NULL;
}



int main() {
    pthread_t thread_id_generador;
    pthread_t thread_id_fibonacci;
    pthread_t thread_id_primo;
    inicializador_semaforo();
    pthread_create(&thread_id_generador, NULL, &hilo_generador_numeros, 0);
    pthread_create(&thread_id_fibonacci, NULL, &hilo_fibonacci, 0);
    pthread_create(&thread_id_primo, NULL, &hilo_primo, 0);
    pthread_join (thread_id_generador, NULL);
    pthread_join (thread_id_fibonacci, NULL);
    pthread_join (thread_id_primo, NULL);
    return  0;
}
