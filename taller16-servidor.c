#include <errno.h>
#include <sys/time.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/file.h>
#include <netinet/in_systm.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netdb.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <string.h>

/*
 * Taller 16:
 *
 * Construir dos programas utilizando sockets
 *
 * Cliente: va a enviar la solicitud de calculo del n-avo fibonacci o el n-avo primo.
 * Servidor: recibe la solicitud, realiza el calculo y envia el resultado de vuelta.
 *
 *
 * */

int *es_primo_i(int *numero){
    int *raiz = (int*) malloc(sizeof(int)*1);
    int *bool= (int*) malloc(sizeof(int)*1);
    int *j= (int*) malloc(sizeof(int)*1);
    *raiz =(int)sqrt((double)*numero);
    *bool = 1;
    for (*j = 2; *j <= *raiz; *j = *j + 1) {
        if(*numero % *j == 0){
            *bool = 0;
            break;
        }
    }
    free(raiz);
    free(j);
    return bool;
}


int *ultimo_numero_primo(int *numero_limite){
    int *i= (int*) malloc(sizeof(int)*1);
    int *j = (int*) malloc(sizeof(int)*1);
    int *raiz = (int*) malloc(sizeof(int)*1);
    int *es_primo = (int*) malloc(sizeof(int)*1);
    *es_primo = 1;
    int *ultimo_primo = (int*) malloc(sizeof(int)*1);
    *ultimo_primo = 1;
    for(*i=1; *i<= *numero_limite; *i = *i + 1){
        es_primo = es_primo_i(i);
        if(*es_primo == 1){
            *ultimo_primo = *i;
        }
    }

    free(i);
    free(j);
    free(raiz);
    free(es_primo);

    return ultimo_primo;
}


int *fibonacci(int *numero){
    int *bool = (int*)malloc(sizeof(int)*1);
    int *ultimo_numero_fibonacci= (int*)malloc(sizeof(int)*1);
    *ultimo_numero_fibonacci = 1;
    int *penultimo_numero_fibonacci= (int*)malloc(sizeof(int)*1);
    *penultimo_numero_fibonacci = 0;
    int *i = (int*)malloc(sizeof(int)*1);
    *bool = 0;
    for(*i = 0; *i <=*numero & *ultimo_numero_fibonacci <= *numero; (*i)++){
        *ultimo_numero_fibonacci = *ultimo_numero_fibonacci + *penultimo_numero_fibonacci;
        *penultimo_numero_fibonacci = *ultimo_numero_fibonacci - *penultimo_numero_fibonacci;
    }
    return ultimo_numero_fibonacci;
}

#define PORT 3550 /* El puerto que será abierto */
#define BACKLOG 2 /* El número de conexiones permitidas */
#define MAXDATASIZE 100


void cleanup(buf)
        char *buf;
{
    int i;
    for(i=0; i<MAXDATASIZE; i++) buf[i]='\0';
}

int main()
{


    int fd, fd2; /* los ficheros descriptores */
    char buf[MAXDATASIZE];
    struct sockaddr_in server;
    /* para la información de la dirección del servidor */

    struct sockaddr_in client;
    /* para la información de la dirección del cliente */

    int sin_size;

    /* A continuación la llamada a socket() */
    if ((fd=socket(AF_INET, SOCK_STREAM, 0)) == -1 ) {
        printf("error en socket()\n");
        exit(-1);
    }

    server.sin_family = AF_INET;

    server.sin_port = htons(PORT);
    /* ¿Recuerdas a htons() de la sección "Conversiones"? =) */

    server.sin_addr.s_addr = INADDR_ANY;
    /* INADDR_ANY coloca nuestra dirección IP automáticamente */

    bzero(&(server.sin_zero),8);
    /* escribimos ceros en el reto de la estructura */


    /* A continuación la llamada a bind() */
    if(bind(fd,(struct sockaddr*)&server,
            sizeof(struct sockaddr))==-1) {
        printf("error en bind() \n");
        exit(-1);
    }

    if(listen(fd,BACKLOG) == -1) {  /* llamada a listen() */
        printf("error en listen()\n");
        exit(-1);
    }

    while(1) {
        sin_size=sizeof(struct sockaddr_in);
        /* A continuación la llamada a accept() */
        if ((fd2 = accept(fd,(struct sockaddr *)&client,(socklen_t *) &sin_size)) == -1) {
            printf("error en accept()\n");
            exit(-1);
        }




        if ( fork() == 0) {
            close (fd);

            printf("Se obtuvo una conexión desde %s\n", inet_ntoa(client.sin_addr) );
            /* que mostrará la IP del cliente */

            send(fd2,"Bienvenido a mi servidor.\n",22,0);
            /* que enviará el mensaje de bienvenida al cliente */

            printf("\n");
            char *cadena_auxiliar_metodo = (char *) malloc(sizeof(char)*10);
            char *metodo_permitido = (char *) malloc(sizeof(char)*10);
            int *parametro_auxiliar = (int *) malloc(sizeof(int)*1);
            while(1) {
                printf("\nSe recibio la siguiente opcion: ");
                if ((int) recv(fd2, buf, 22, 0) == -1) {
                    printf("\nHubo un error al recibir el mensaje, se va a cerrar la conexion.");
                    close(fd2);
                    break;
                }

                if(buf[0] == '\0'){
                    close(fd2);
                    break;
                }

                printf("%s", buf);

                cadena_auxiliar_metodo = strtok (buf, " ");
                *parametro_auxiliar =atoi(strtok (NULL, " "));

                sprintf(metodo_permitido,"fibonacci");

                printf("\n %s \n",cadena_auxiliar_metodo);

                if(strcmp(cadena_auxiliar_metodo,metodo_permitido) == 0){
                    printf("El metodo es fibonacci");
                    cleanup(buf);
                    sprintf(buf,"El fibonacci es: %d",*fibonacci(parametro_auxiliar));
                } else{
                    sprintf(metodo_permitido,"primo");
                    if(strcmp(cadena_auxiliar_metodo,metodo_permitido) == 0){
                        printf("El metodo es primo");
                        cleanup(buf);
                        sprintf(buf,"El ultimo primo es: %d",*ultimo_numero_primo(parametro_auxiliar));
                    }
                }

                send(fd2,buf,22,0);
                printf("\n Se a enviado respuesta al cliente. %s", buf);
                cleanup(buf);
            }
        }


        close(fd2);
    }
}


