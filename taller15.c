#include <stdlib.h>
#include <malloc.h>
#include <pthread.h>
#include <unistd.h>
#include <math.h>

/*
 * Taller 15:
 *
 *  Hacer el taller 13 con variables de condicion.
 * */


int *es_primo_i(int *numero){
    int *raiz = (int*) malloc(sizeof(int)*1);
    int *bool= (int*) malloc(sizeof(int)*1);
    int *j= (int*) malloc(sizeof(int)*1);
    *raiz =(int)sqrt((double)*numero);
    *bool = 1;
    for (*j = 2; *j <= *raiz; *j = *j + 1) {
        if(*numero % *j == 0){
            *bool = 0;
            break;
        }
    }
    free(raiz);
    free(j);
    return bool;
}


int *ultimo_numero_primo(int *numero_limite){
    int *i= (int*) malloc(sizeof(int)*1);
    int *j = (int*) malloc(sizeof(int)*1);
    int *raiz = (int*) malloc(sizeof(int)*1);
    int *es_primo = (int*) malloc(sizeof(int)*1);
    *es_primo = 1;
    int *ultimo_primo = (int*) malloc(sizeof(int)*1);
    *ultimo_primo = 1;
    for(*i=1; *i<= *numero_limite; *i = *i + 1){
        es_primo = es_primo_i(i);
        if(*es_primo == 1){
            *ultimo_primo = *i;
        }
    }

    free(i);
    free(j);
    free(raiz);
    free(es_primo);

    return ultimo_primo;
}


int *fibonacci(int *numero){
    int *bool = (int*)malloc(sizeof(int)*1);
    int *ultimo_numero_fibonacci= (int*)malloc(sizeof(int)*1);
    *ultimo_numero_fibonacci = 1;
    int *penultimo_numero_fibonacci= (int*)malloc(sizeof(int)*1);
    *penultimo_numero_fibonacci = 0;
    int *i = (int*)malloc(sizeof(int)*1);
    *bool = 0;
    for(*i = 0; *i <=*numero & *ultimo_numero_fibonacci <= *numero; (*i)++){
        *ultimo_numero_fibonacci = *ultimo_numero_fibonacci + *penultimo_numero_fibonacci;
        *penultimo_numero_fibonacci = *ultimo_numero_fibonacci - *penultimo_numero_fibonacci;
    }
    return ultimo_numero_fibonacci;
}


struct numeros{
    struct numeros *siguiente;
    int *numero;
};

struct numeros *lista_numeros;

int *bandera_lista_numeros;
pthread_mutex_t *mutex_lista_numero;


void inicializador(){
    bandera_lista_numeros = (int*) malloc(sizeof(int));
    *bandera_lista_numeros = 0;
    mutex_lista_numero = (pthread_mutex_t*) malloc(sizeof(pthread_mutex_t));
    pthread_mutex_init (mutex_lista_numero, NULL);
}

int *generar_numero_aletorio(){
    int *numero_aletorio = (int *) malloc(sizeof(int)*1);
    *numero_aletorio = rand()%50;
    return numero_aletorio;
}


void *hilo_generador_numeros(void *args){
    struct numeros *numeros_auxiliares;
    int *running = (int*)malloc(sizeof(int)*1);
    *running = 1;
    while(*running){
        pthread_mutex_lock (mutex_lista_numero);
        if(lista_numeros != NULL){
            if(lista_numeros->siguiente == NULL){
                numeros_auxiliares = (struct numeros*)malloc(sizeof(struct numeros));
                lista_numeros->siguiente = numeros_auxiliares;
                numeros_auxiliares->numero = generar_numero_aletorio();
            }
        }else{
            numeros_auxiliares = (struct numeros*)malloc(sizeof(struct numeros));
            lista_numeros = numeros_auxiliares;
            lista_numeros->numero = generar_numero_aletorio();
        };
        pthread_mutex_unlock(mutex_lista_numero);
        sleep(1);
    }
    return NULL;
}

void *hilo_fibonacci(void* args){
    struct numeros *numeros_auxiliares;
    int *running = (int*)malloc(sizeof(int)*1);
    int *numero_auxiliar= (int*)malloc(sizeof(int));
    int *contador= (int*)malloc(sizeof(int));
    *contador = 0;
    *running = 1;
    while(*running){
        pthread_mutex_lock (mutex_lista_numero);
        if(lista_numeros != NULL){
            if(*bandera_lista_numeros == 0){
                if(*contador==2){
                    *bandera_lista_numeros = 1;
                    *contador = 0;
                } else{
                    (*contador)++;
                }
            }
            *numero_auxiliar = *lista_numeros->numero;
            printf("\n El fibonacci del numero %d es %d",*numero_auxiliar, *fibonacci(numero_auxiliar));
            numeros_auxiliares = lista_numeros;
            if(lista_numeros->siguiente != NULL){
                lista_numeros = lista_numeros->siguiente;
            }else {
                lista_numeros = NULL;
            }
            free(numeros_auxiliares);
        }
        pthread_mutex_unlock(mutex_lista_numero);
        sleep(1);
    }
    return NULL;
}

void *hilo_primo(void* args){
    struct numeros *numeros_auxiliares;
    int *numero_auxiliar = (int*)malloc(sizeof(int));
    int *result = (int*)malloc(sizeof(int));
    int *running = (int*)malloc(sizeof(int)*1);
    *running = 1;
    while (*running){
        pthread_mutex_lock (mutex_lista_numero);
        if(lista_numeros != NULL){
            if(*bandera_lista_numeros == 1) {
                *numero_auxiliar = *lista_numeros->numero;
                result = ultimo_numero_primo(numero_auxiliar);
                printf("\n El primo del numero %d es %d", *numero_auxiliar, *result);
                if (lista_numeros->siguiente != NULL) {
                    numeros_auxiliares = lista_numeros;
                    lista_numeros = lista_numeros->siguiente;
                    free(numeros_auxiliares);
                }
                numeros_auxiliares = lista_numeros;
                lista_numeros = NULL;
                free(result);
                *bandera_lista_numeros = 0;

            }
        }
        pthread_mutex_unlock(mutex_lista_numero);
    }
    return NULL;
}



int main() {
    pthread_t thread_id_generador;
    pthread_t thread_id_fibonacci;
    pthread_t thread_id_primo;
    inicializador();
    printf("\n Esta applicacion de hilos imprime 3 fibonaci antes de habilitar la impresion del hilo primo, haciendo"
           "uso de una variable de condicion");
    pthread_create(&thread_id_generador, NULL, &hilo_generador_numeros, 0);
    pthread_create(&thread_id_fibonacci, NULL, &hilo_fibonacci, 0);
    pthread_create(&thread_id_primo, NULL, &hilo_primo, 0);
    pthread_join (thread_id_generador, NULL);
    pthread_join (thread_id_fibonacci, NULL);
    pthread_join (thread_id_primo, NULL);
    return  0;
}

