//
// Created by noescobar on 17/07/15.
//

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include "lector_archivos.h"

#define MAXSIZECHAR 1024


char *cargar_archivo(char *nombre){
    FILE *file;
    size_t nread;
    char *archivo = malloc(1);
    char *intermedio = malloc(MAXSIZECHAR);
    char *auxiliar;
    int i = 1;

    file = fopen(nombre, "r");
    if (file) {

        while ((nread = fread(intermedio, 1, MAXSIZECHAR, file)) > 0) {
            auxiliar = archivo;
            archivo = malloc(sizeof(char)*MAXSIZECHAR*i);
            sprintf(archivo,"%s%s",auxiliar,intermedio);
            free(auxiliar);
            i++;
        }
        fclose(file);
    } else{
        free(archivo);
        archivo = NULL;
    }

    free(intermedio);

    return  archivo;

}


int main( int argc, char *argv[]) {
    printf("%d",(int)strlen(cargar_archivo(argv[1])));
    return 1;
}