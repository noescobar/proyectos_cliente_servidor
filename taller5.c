#include <stdio.h>
// #include <math.h>
#include <malloc.h>
#include <unistd.h>
#include <sys/types.h>

/*
 * Taller 5:
 *
 * Modificar el taller 4 de manera que los procesos hijos llamen cada uno a un programa:
 *
 *  PS2: proceso tablas.
 *  PS3: proceso primos.
 *
 * Nota: utilizando una de las funciones de la familia exec.
 * */


void *crear_proceso(void (*funcion)()){
    int *proceso_hijo;
    proceso_hijo = (int*) malloc(sizeof(int));
    *proceso_hijo = fork();
    switch (*proceso_hijo){
            case -1:
                printf("\n Ha sucedido un error y no puede iniciarse el hilo");
                break;
            case 0:
                funcion();
            break;
            default:
                break;
        }
    return NULL;
}

void proceso_1(){
    char* arg_list[] = {
            "./taller5multiplicacion",
            NULL
    };
    execvp("./taller5multiplicacion", arg_list);
    //free(arg_list);
    printf("Un error a ocurrido al ejecutar el proceso multiplicacion");
}

void proceso_2(){
    char* arg_list[] = {
            "./taller5primos",
            NULL
    };
    execvp("./taller5primos", arg_list);
    printf("Un error a ocurrido al ejecutar el proceso primos");
    //free(arg_list);
}


int main() {

    crear_proceso(&proceso_1);
    crear_proceso(&proceso_2);

    return  0;
}
