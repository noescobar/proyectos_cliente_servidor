#include <sys/time.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <string.h>
#include <termios.h>
#include <pthread.h>



#define MAXHOSTNAME 80
#define BUFSIZE 1024
#define DEFAULT_HOST "127.0.0.1";
#define DEFAULT_PORT "5000";

char buf[BUFSIZE];
char rbuf[BUFSIZE];
char *username;
struct sockaddr_in from_global;
int mensaje_recibido;

char *simbolo_entrada_terminal(char *username, char *host){
    char *simbolo_terminal = (char *)malloc(sizeof(char)*1024);
    if(username == NULL) {
        sprintf(simbolo_terminal,"\n[$anonimo@%s]$",host);
    }else{
        sprintf(simbolo_terminal,"\n[%s@%s]$", username, host);
    }
    return simbolo_terminal;
}


void cleanup(char *buf){
    int i;
    for(i=0; i<BUFSIZE; i++) buf[i]='\0';
}


static struct termios old, nuevo;

/* Initialize new terminal i/o settings */
void initTermios(int echo)
{
    tcgetattr(0, &old); /* grab old terminal i/o settings */
    nuevo = old; /* make new settings same as old settings */
    nuevo.c_lflag &= ~ICANON; /* disable buffered i/o */
    nuevo.c_lflag &= echo ? ECHO : ~ECHO; /* set echo mode */
    tcsetattr(0, TCSANOW, &nuevo); /* use these new terminal i/o settings now */
}

/* Restore old terminal i/o settings */
void resetTermios(void)
{
    tcsetattr(0, TCSANOW, &old);
}

/* Read 1 character - echo defines echo mode */
char getch_(int echo)
{
    char ch;
    initTermios(echo);
    ch = getchar();
    resetTermios();
    return ch;
}

/* Read 1 character without echo */
char getch(void)
{
    return getch_(0);
}



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                                                     *
 *  funcion scantf_hidden:                                                                                             *
 *      descripcion: Esta funcion me permite leer por teclado teniendo como restriccion que lo que el usuario escriba  *
 *                   no sera mostrado por pantala y sera reemplazado por un character previamente dado                 *
 *      parametros:                                                                                                    *
 *          *key : En caso de que se utilice una llave para describir el contenido que se va a leer                    *
 *           symbol : Este es el simbolo por el que se va a reemplazar lo que digite el usuario                        *
 *                                                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*/

char *scantf_hidden(char *key, const char symbol){
    char *cadena=(char *)malloc(sizeof(char)*40);
    int i=0;
    printf("\n%s",key);
    while(cadena[i]!=10){
        //Capturamos carácter
        cadena[i]=getch();

        //Si se pulsa la tecla RETROCESO, se retrocede un carácter, se imprime un espacio para eliminar el asterisco y
        //se vuelve a retroceder para que el siguiente asterisco se coloque a continuación del anterior.
        if (cadena[i] == 127 ) {
            if(i > 0) {
                putchar(8);
                putchar(' ');
                putchar(8);
                i--;
            }
        }else{
            //Si es un carácter válido y no se ha sobrepasado el límite de 20 caracteres se imprime un asterisco
            if(cadena[i]>32 && i<20) {
                putchar(symbol);
                i++;
            }
        }
    }
    return cadena;
}


char **split ( char *string, const char sep) {

    char **lista;
    char *p = string;
    int i = 0;

    int pos;
    const int len = (int)strlen (string);

    lista = (char **) malloc (sizeof (char *));
    if (lista == NULL) { /* Cannot allocate memory */
        return NULL;
    }

    lista[pos=0] = NULL;

    while (i < len) {

        while ((p[i] == sep) && (i < len))
            i++;

        if (i < len) {

            char **tmp = (char **) realloc (lista , (pos + 2) * sizeof (char *));
            if (tmp == NULL) { /* Cannot allocate memory */
                free (lista);
                return NULL;
            }
            lista = tmp;
            tmp = NULL;

            lista[pos + 1] = NULL;
            lista[pos] = (char *) malloc (sizeof (char));
            if (lista[pos] == NULL) { /* Cannot allocate memory */
                for (i = 0; i < pos; i++)
                    free (lista[i]);
                free (lista);
                return NULL;
            }

            int j = 0;
            for (i; ((p[i] != sep) && (i < len)); i++) {
                lista[pos][j] = p[i];
                j++;

                char *tmp2 = (char *) realloc (lista[pos],(j + 1) * sizeof (char));
                if (lista[pos] == NULL) { /* Cannot allocate memory */
                    for (i = 0; i < pos; i++)
                        free (lista[i]);
                    free (lista);
                    return NULL;
                }
                lista[pos] = tmp2;
                tmp2 = NULL;
            }
            lista[pos][j] = '\0';
            pos++;
        }
    }

    return lista;
}

int conectar_servidor(struct sockaddr_in *from_pointer, char *argv[], int argc){
    int sd;
    struct sockaddr_in server;
    struct sockaddr_in from;
    struct hostent *hp, *gethostbyname();
    struct servent *sp;
    struct sockaddr_in addr;
    int fromlen;
    char ThisHost[80];
    char *host;
    char *port;
    sp = getservbyname("ruleta", "tcp");
    /* get TCPClient Host information, NAME and INET ADDRESS */

    gethostname(ThisHost, MAXHOSTNAME);

    if(argc <2){
        printf("\nInstrucciones de uso: %s <direccion ip servidor> <puerto servidor>", argv[0]);
        printf("\nSe utilizara la direccion ip por defecto.");
        host = DEFAULT_HOST;
    }else{
        host = argv[1];
    }

    if(argc <3){
        printf("\nInstrucciones de uso: %s  %s  <puerto servidor>", argv[0], host);
        printf("\nSe utilizara el puerto por defecto.");
        port = DEFAULT_PORT;
    } else{
        host = argv[1];
        port = argv[2];
    }

    /* OR strcpy(ThisHost,"localhost"); */

    printf("\nEl cliente esta corriendo con nombre: %s\n", ThisHost);
    if ( (hp = gethostbyname(ThisHost)) == NULL ) {
        fprintf(stderr, "No se puede encontrar el host %s\n", host);
        exit(-1);
    }
    bcopy ( hp->h_addr, &(server.sin_addr),(socklen_t) hp->h_length);
    //printf(" (TCP/Cleint INET ADDRESS is: %s )\n", inet_ntoa(server.sin_addr));

    /* get TCP/Server Host information, NAME and INET ADDRESS */

    if ( (hp = gethostbyname(host)) == NULL ) {
        addr.sin_addr.s_addr = inet_addr(host);
        if ((hp = gethostbyaddr(&addr.sin_addr.s_addr,
                                sizeof(addr.sin_addr.s_addr),AF_INET)) == NULL) {
            fprintf(stderr, "Can't find host %s\n", host);
            exit(-1);
        }
    }
    //printf("----TCP/Server running at host NAME: %s\n", hp->h_name);
    bcopy ( hp->h_addr, &(server.sin_addr),(size_t) hp->h_length);
    //printf(" (TCP/Server INET ADDRESS is: %s )\n",inet_ntoa(server.sin_addr));

    /* Construct name of socket to send to. */
    server.sin_family = (sa_family_t) hp->h_addrtype;
    /* server.sin_family = AF_INET; */

    server.sin_port = htons((uint16_t) atoi(port));
    /*OR server.sin_port = sp->s_port; */

    /* Create socket on which to send and receive */

    sd = socket (hp->h_addrtype,SOCK_STREAM,0);
    /*OR sd = socket (PF_INET,SOCK_STREAM,0); */

    if (sd<0) {
        perror("opening stream socket");
        exit(-1);
    }
    /* Connect to TCP/SERVER */
    if ( connect(sd,(struct sockaddr *) &server, sizeof(server)) < 0 ) {
        close(sd);
        perror("connecting stream socket");
        exit(0);
    }
    fromlen = sizeof(from);
    if (getpeername(sd, (struct sockaddr *)&from, (socklen_t *)&fromlen)<0){
        perror("could't get peername\n");
        exit(1);
    }
    printf("Conectado con el servidor TCP:");
    printf("%s:%d\n", inet_ntoa(from.sin_addr),
           ntohs(from.sin_port));
    if ((hp = gethostbyaddr(&from.sin_addr.s_addr,
                            sizeof(from.sin_addr.s_addr),AF_INET)) == NULL)
        fprintf(stderr, "No se puede encontrar el host %s\n", inet_ntoa(from.sin_addr));
    else
        printf("(El nombre es : %s)\n", hp->h_name);


    *from_pointer = from;

    return sd;
}