#include "core_cliente.h"

char *leer_teclado(char *key, char* tipo, const char *simbolo){
    char *cadena_retorno = (char *)malloc(sizeof(char)*1024);
    if(tipo != NULL){
        if(strcmp(tipo,"h")==0){
            cadena_retorno = scantf_hidden(key,simbolo[0]);
        }
    } else{
        printf("\n%s",key);
        fgets(cadena_retorno,1024,stdin);

    }

    return  cadena_retorno;

};


void *hilo_lector_teclado(void *socket){
    mensaje_recibido = 1;
    int *socket_pointer = (int *) socket;
    int socket_id = *socket_pointer;
    char *buf_auxiliar = (char*) malloc(sizeof(char)*BUFSIZE);
    char **parametros_from_server;
    char **parametros_cliente;
    int i=0;
    while(1){
        if(mensaje_recibido ==1) {
            if( rbuf[0] != '$'){
                cleanup(buf);
                sprintf(buf, "%s", leer_teclado(simbolo_entrada_terminal(username, inet_ntoa(from_global.sin_addr)), NULL, NULL));
                if (buf[0] == '\0') {
                    break;
                }
                parametros_cliente = split(buf, ' ');

                if(strstr(parametros_cliente[0],"clear") != NULL){
                    system("clear");
                    buf[0] = '\n';
                }
            }
            else if(rbuf[0] == '$'){
                sprintf(buf_auxiliar,"%s", rbuf);
                cleanup(rbuf);
                parametros_from_server = split(buf_auxiliar, ' ');
                if(strcmp(parametros_from_server[1],"-h")==0){
                    i = 3;
                    sprintf(buf_auxiliar,"");
                    while(parametros_from_server[i]!= NULL){
                        if(i != 3){
                            sprintf(buf_auxiliar,"%s ",buf_auxiliar);
                        }
                        sprintf(buf_auxiliar,"%s%s",buf_auxiliar,parametros_from_server[i]);
                        i++;
                    }
                    sprintf(buf,"%s",scantf_hidden(buf_auxiliar,'*'));
                }else{
                    i = 1;
                    sprintf(buf_auxiliar,"");
                    while(parametros_from_server[i]!= NULL){
                        if(i != 1){
                            sprintf(buf_auxiliar,"%s ",buf_auxiliar);
                        }
                        sprintf(buf_auxiliar,"%s%s",buf_auxiliar,parametros_from_server[i]);
                        i++;
                    }
                    sprintf(buf, "%s", leer_teclado(buf_auxiliar, NULL, NULL));
                }
            }
            if(buf[0] != '\n' & rbuf[0] != '$') {
                if (send(socket_id, buf, (size_t) 1024, 0) > 0) {
                    //printf("\nEnviando mensaje");
                    mensaje_recibido = 0;
                }
            }
        }
    }
    close(socket_id);
    return NULL;
}


void *hilo_escuchador(void *socket){
    int *socket_pointer = (int *) socket;
    int socket_id = *socket_pointer;
    char **parametros;
    int cc;
    while (1){
        if ((cc= (int) recv(socket_id, rbuf, sizeof(rbuf), 0)) > 0) {
            if(rbuf [0] != '$'){
                if(rbuf [0] == '\0'){
                    printf("\nSe ha perdido la conexion con el servidor");
                    break;
                }else {
                    if(rbuf[0] == '-'){
                        parametros = split(rbuf, ' ');
                        printf(parametros[0]);
                        if(strcmp(parametros[0],"-u") == 0){
                            username = parametros[1];
                        }

                    }else {
                        printf("\n%s\n", rbuf);
                    }
                }
            }
            mensaje_recibido = 1;
        } else{
            printf("\nSe a perdido la conexion con el servidor");
            break;
        }
    }
    return  NULL;
}

