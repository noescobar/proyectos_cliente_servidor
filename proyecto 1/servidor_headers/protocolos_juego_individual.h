#include "utilidades_servidor.h"


void protocolo_adivinar_individual(Usuario *usuario, char **parametros){
    int *i = (int *)malloc(sizeof(int)*1);
    *i = 1;
    char *frase_parametro= (char *)malloc(sizeof(char)*1024);
    char *cadena_mensaje= (char *)malloc(sizeof(char)*1024);
    char *tablero;
    sprintf(frase_parametro,"");
    while(parametros[*i]!=NULL){
        sprintf(frase_parametro,"%s%s",frase_parametro,parametros[*i]);
        (*i)++;
        if(parametros[*i]!=NULL){
            sprintf(frase_parametro,"%s ",frase_parametro);
        }
    }
    printf("\n intentos --> %d", *usuario->intentos);
    if(comprobar_partida(usuario,frase_parametro)){
        *usuario->intentos = *usuario->intentos + 1;
        aumentar_puntaje_usuario(usuario);
        tablero = mostrar_partida(usuario->partida_actual);
        sprintf(cadena_mensaje,"\n\nHas adivinado la frase\n %s", tablero);
        mandar_mensaje(usuario->socket_id,cadena_mensaje);
    } else{
        *usuario->intentos = *usuario->intentos + 1;
        tablero = mostrar_partida(usuario->partida_actual);
        sprintf(cadena_mensaje,"\n\nTe has equivocado la frase\n %s", tablero);
        mandar_mensaje(usuario->socket_id,cadena_mensaje);
    }
    free(i);
    free(cadena_mensaje);
    free(frase_parametro);
}


void protocolo_vocal_individual(Usuario *usuario, char **parametros){
    Partida *partida_actual = usuario->partida_actual;
    char *tablero;
    char *mensaje = (char *)malloc(sizeof(char)*1024);
    char *segundo_parametro = (char *)malloc(sizeof(char)*1);
    if(*usuario->puntaje_actual >= 50){
        if(parametros[1] != NULL){
            *usuario->intentos = *usuario->intentos + 1;
            *usuario->puntaje_actual = *usuario->puntaje_actual - 50;
            sprintf(segundo_parametro,"%c",parametros[1][0]);
            if(strstr(partida_actual->letras_jugadas, segundo_parametro) == NULL){
                sprintf(partida_actual->letras_jugadas,"%s%s",partida_actual->letras_jugadas, segundo_parametro);
                if (strstr(partida_actual->frase, segundo_parametro) != NULL) {
                    sprintf(partida_actual->letras_acertadas,"%s%s",partida_actual->letras_acertadas, segundo_parametro);
                    if(comprobar_partida(usuario,NULL)) {
                        aumentar_puntaje_usuario(usuario);
                        tablero = mostrar_partida(partida_actual);
                        sprintf(mensaje,"\n Has acertado la vocal %s has ganado la partida\n %s",segundo_parametro,tablero);
                        mandar_mensaje(usuario->socket_id,mensaje);
                     }else{
                        tablero = mostrar_partida(partida_actual);
                        sprintf(mensaje,"\n Has acertado la vocal %s\n %s",segundo_parametro,tablero);
                        mandar_mensaje(usuario->socket_id,mensaje);
                    }
                }else{
                    tablero = mostrar_partida(partida_actual);
                    sprintf(mensaje,"\n Te has equivocado con la vocal %s pierdes turno\n %s",segundo_parametro,tablero);

                    mandar_mensaje(usuario->socket_id,mensaje);
                }
            }else{
                tablero = mostrar_partida(partida_actual);
                sprintf(mensaje,"\nLa vocal %s ya ha sido utilizada %s",segundo_parametro,tablero);
                mandar_mensaje(usuario->socket_id,mensaje);
            }
        }
    } else{
        mandar_mensaje(usuario->socket_id,"\n\nNo puedes usar una vocal, no posees los puntos suficientes para usarla");
    }
}


void protocolo_consonante_individual(Usuario *usuario, char **parametros){
    Partida *partida_actual = usuario->partida_actual;
    char *tablero;
    char *mensaje = (char *)malloc(sizeof(char)*1024);
    char *segundo_parametro = (char *)malloc(sizeof(char)*1);
    if(parametros[1] != NULL){
        if(es_vocal(parametros[1]) == 1){
            mandar_mensaje(
                    usuario->socket_id,"\nNo ingreso una consonante valida."
            );
        }else {
            sprintf(segundo_parametro, "%c", parametros[1][0]);
            *usuario->intentos = *usuario->intentos + 1;
            if (strstr(partida_actual->letras_jugadas, segundo_parametro) == NULL) {
                sprintf(partida_actual->letras_jugadas, "%s%s", partida_actual->letras_jugadas, segundo_parametro);
                if (strstr(partida_actual->frase, segundo_parametro) != NULL) {
                    sprintf(partida_actual->letras_acertadas, "%s%s", partida_actual->letras_acertadas,
                            segundo_parametro);
                    if (comprobar_partida(usuario, NULL)) {
                        aumentar_puntaje_usuario(usuario);
                        tablero = mostrar_partida(partida_actual);
                        sprintf(mensaje, "\nHas acertado la consonante %s y has ganado la partida\n %s",
                                segundo_parametro, tablero);
                        mandar_mensaje(usuario->socket_id, mensaje);
                    } else {
                        tablero = mostrar_partida(partida_actual);
                        sprintf(mensaje, "\nHas acertado la consonante %s\n %s", segundo_parametro, tablero);
                        mandar_mensaje(usuario->socket_id, mensaje);
                    }
                } else {
                    tablero = mostrar_partida(partida_actual);
                    sprintf(mensaje, "\nTe has equivocado con la consonante %s pierdes turno\n %s", segundo_parametro,
                            tablero);
                    mandar_mensaje(usuario->socket_id, mensaje);
                }
            } else {
                tablero = mostrar_partida(partida_actual);
                sprintf(mensaje, "\nLa consonante %s ya ha sido utilizada %s", segundo_parametro, tablero);
                mandar_mensaje(usuario->socket_id, mensaje);
            }
        }
    } else{
        mandar_mensaje(usuario->socket_id,"\n\nNo ha proporporcionado ningun segundo parametro.\n\n");
    }
}