#include <pthread.h>


struct Partida{
    int *id;
    char *frase;
    char letras_jugadas[27];
    char letras_acertadas[27];
    char *nombre_partida;
    char *tipo_partida;
    struct Usuario *ganador;
    int iniciada;
    int turno;
    pthread_mutex_t mutex_finalizar;
    struct ListaUsuarios *jugadores;
};
typedef struct Partida Partida;


struct ListaPartidas {
    struct ListaPartidaObject *cabeza;
    struct ListaPartidaObject *cola;
    int tamano;
};
typedef struct ListaPartidas ListaPartidas;

struct ListaPartidaObject {
    ListaPartidas *cabecera;
    struct ListaPartidaObject *anterior;
    struct ListaPartidaObject *siguiente;
    Partida *partida_object;
};
typedef struct ListaPartidaObject ListaPartidaObject;

struct Usuario{
    int *id;
    char username[30];
    Partida *partida_actual;
    int *puntaje_actual;
    int *intentos;
    int *socket_id;
};
typedef struct Usuario Usuario;


struct ListaUsuarios {
    struct ListaUsuarioObject *cabeza;
    struct ListaUsuarioObject *cola;
    int tamano;
};
typedef struct ListaUsuarios ListaUsuarios;

struct ListaUsuarioObject{
    ListaUsuarios *cabecera;
    struct ListaUsuarioObject *anterior;
    struct ListaUsuarioObject *siguiente;
    Usuario *usuario;
};
typedef struct ListaUsuarioObject ListaUsuarioObject;



void agregar_partida(ListaPartidas *lista,Partida *objeto){
    ListaPartidaObject *objecto_lista = (ListaPartidaObject *) malloc(sizeof(ListaPartidaObject)*1);
    objecto_lista->partida_object = objeto;

    if(lista->cabeza == NULL){
        if(lista->cola != NULL){
            free(lista->cola);
            lista->cola = NULL;
        }
        lista->cabeza = objecto_lista;
        lista->cola = objecto_lista;
        lista->tamano = 1;
        objecto_lista->cabecera = lista;
        objecto_lista->anterior = NULL;
        objecto_lista->siguiente = NULL;
    }else{
        if(lista->cola != NULL){
            lista->cola->siguiente = objecto_lista;
            objecto_lista->anterior = lista->cola;
            lista->cola = objecto_lista;
            objecto_lista->siguiente = NULL;
            lista->tamano++;
        }
    }
}


void remover_partida(ListaPartidas *lista,Partida *objeto){
    ListaPartidaObject *objecto_lista =lista->cabeza;
    while(objecto_lista != NULL){
        if(objeto == objecto_lista->partida_object){
            if(objecto_lista == lista->cabeza){
                lista->cabeza = objecto_lista->siguiente;
            }
            if(objecto_lista == lista->cola){
                lista->cola = objecto_lista->anterior;
            }
            if(objecto_lista->anterior != NULL){
                objecto_lista->anterior->siguiente = objecto_lista->siguiente;
            }
            if(objecto_lista->siguiente != NULL){
                objecto_lista->siguiente->anterior = objecto_lista->anterior;
            }
            lista->tamano --;
            break;
        }
        objecto_lista = objecto_lista->siguiente;
    }
}

void agregar_usuario(struct ListaUsuarios *lista,Usuario *objeto){
    ListaUsuarioObject *objecto_lista = (ListaUsuarioObject *) malloc(sizeof(ListaUsuarioObject));
    objecto_lista->usuario = objeto;

    if(lista->cabeza == NULL){
        if(lista->cola != NULL){
            free(lista->cola);
            lista->cola = NULL;
        }
        lista->cabeza = objecto_lista;
        lista->cola = objecto_lista;
        objecto_lista->cabecera = lista;
        objecto_lista->cabecera = lista;
        objecto_lista->anterior = NULL;
        objecto_lista->siguiente = NULL;
        lista->tamano = 1;
    }else{
        if(lista->cola != NULL){
            lista->cola->siguiente = objecto_lista;
            objecto_lista->anterior = lista->cola;
            lista->cola = objecto_lista;
            objecto_lista->siguiente = NULL;
            lista->tamano++;
        }
    }
}


void remover_usuario(ListaUsuarios *lista,Usuario *objeto){
    ListaUsuarioObject *objecto_lista =lista->cabeza;
    while(objecto_lista != NULL){
        if(objeto == objecto_lista->usuario){
            if(objecto_lista == lista->cabeza){
                lista->cabeza = objecto_lista->siguiente;
            }
            if(objecto_lista == lista->cola){
                lista->cola = objecto_lista->anterior;
            }
            if(objecto_lista->anterior != NULL){
                objecto_lista->anterior->siguiente = objecto_lista->siguiente;
            }
            if(objecto_lista->siguiente != NULL){
                objecto_lista->siguiente->anterior = objecto_lista->anterior;
            }
            lista->tamano --;
            break;
        }
        objecto_lista = objecto_lista->siguiente;
    }
}



/*
 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
 *  funcion mostrar_usuario_list:                                                                                      *
 *                                                                                                                     *
 *      descripcion: Como se puede ver en el nombre esta funcion me permite mostrar los usuario contenidos en una n de *
 *                   estructura de  tipo ListaUsuarios, esta funcion me permite de forma simple comprobar los usuarios *
 *                   dentro de una lista.                                                                              *                                           *
 *                                                                                                                     *
 *      Parametros:                                                                                                    *
 *          lista: Esta  es  la  lista  que  contiene  a los usuario siendo esta la que recorreremos para mostrar cada *
 *                 contenido en esta misma.                                                                            *
 *                                                                                                                     *
 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
*/

void mostrar_usuario_list(ListaUsuarios *lista){
    ListaUsuarioObject *lista_auxiliar = lista->cabeza;
    while(lista_auxiliar!= NULL){
        printf("\n");
        printf("%i \t",*lista_auxiliar->usuario->id);
        printf("%s \t", lista_auxiliar->usuario->username);
        printf("\n");
        lista_auxiliar = lista_auxiliar->siguiente;
    }
}
