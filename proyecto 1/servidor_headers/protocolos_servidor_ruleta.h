#include "protocolos_juego_grupal.h"


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                                                     *
 *                                                                                                                     *
 *                                                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*/

void protocolo_de_registro(char **parametros_de_entrada, int *socket){
    char *username=NULL;
    char *password=NULL;
    char **parametros;
    int *tamano_parametros= (int *) malloc(sizeof(int)*1);
    *tamano_parametros = get_len(parametros_de_entrada);
    if(*tamano_parametros >= 3){
        if(strcmp(parametros_de_entrada[1],"-u")){
            username = parametros_de_entrada[2];
        }
    }
    while (1){
        if(username == NULL){
            parametros = hacer_peticion(socket,"$ Ingresar Usuario:");
            *tamano_parametros = get_len(parametros);
            if(*tamano_parametros >= 2){
                if(strcmp(parametros[0],"username")){
                    username = parametros[1];
                }
            } else{
                username = parametros[0];
            }
        }else if(password == NULL) {
            parametros = hacer_peticion(socket, "$ -h *Ingresar password:");
            *tamano_parametros = get_len(parametros);
            if(*tamano_parametros >= 2){
                if(strcmp(parametros[0],"password")){
                    password = parametros[1];
                }
            } else if (*tamano_parametros == 1){
                password = parametros[0];
            }
        } else{
            break;
        }
    }
    if(registrar_usuario(conector_global, username, password)){
        mandar_mensaje(socket,"\n Registro con exito del usuario");
    }else{
        mandar_mensaje(socket,"\n Ha habido un error, intentelo de nuevo con otro nombre de usuario.");
    }

}


Partida *protocolo_crear_partida_individual(char **parametros, Usuario *usuario_actual){
    Partida *partida_resultante=NULL;
    ListaUsuarioObject *lista_usuario_object = NULL;
    char *cadena_auxiliar = (char *) malloc(sizeof(char)*1024);
    sprintf(cadena_auxiliar,"");
    partida_resultante = crear_partida("individual");
    lista_usuario_object = (ListaUsuarioObject *) malloc(sizeof(ListaUsuarioObject));
    partida_resultante->jugadores->cabeza = lista_usuario_object;
    partida_resultante->jugadores->cola = lista_usuario_object;
    partida_resultante->jugadores->tamano = 1;
    lista_usuario_object->usuario =usuario_actual;
    lista_usuario_object->anterior = NULL;
    lista_usuario_object->siguiente = NULL;
    lista_usuario_object->cabecera =partida_resultante->jugadores;
    agregar_partida(partidas_individuales, partida_resultante);
    partida_resultante->iniciada = 1;
    mandar_mensaje(
            usuario_actual->socket_id,
            "\n\nSe ha creado una nueva partida individual, puedes empezar el juego. \n\n"

    );
    return partida_resultante;
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                                                     *
 *                                                                                                                     *
 *                                                                                                                     *
 *                                                                                                                     *
 *                                                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*/

Partida *protocolo_iniciar_partida(char **parametros, Usuario *usuario_actual){
    Partida *partida_resultante=NULL;
    ListaUsuarioObject *lista_usuario_object = NULL;
    char *cadena_auxiliar = (char *) malloc(sizeof(char)*1024);
    sprintf(cadena_auxiliar,"");
    if(get_len(parametros)>=2){
        if(strcmp(parametros[1],"-i")==0){
            partida_resultante = protocolo_crear_partida_individual(parametros, usuario_actual);
        } else if(strcmp(parametros[1],"-g")==0){
            if(partidas_grupales->cola !=NULL){
                if(partidas_grupales->cola->partida_object->jugadores->tamano < 3){
                    partida_resultante = partidas_grupales->cola->partida_object;
                    agregar_usuario(partida_resultante->jugadores, usuario_actual);
                    sprintf(cadena_auxiliar,"\n\n Se ha unido el usuario: %s \n\n",usuario_actual->username);
                    broad_cast_mensaje(partida_resultante->jugadores,cadena_auxiliar,usuario_actual);

                    if(partidas_grupales->cola->partida_object->jugadores->tamano == 3){
                        broad_cast_mensaje(
                                partida_resultante->jugadores, "\n\n El juego puede empezar. \n\n", NULL
                        );
                        partida_resultante->iniciada = 1;
                    } else{
                        partida_resultante->iniciada = 0;
                        mandar_mensaje(usuario_actual->socket_id,"\n Debes esperar a una persona mas.");
                    }

                }else{
                    partida_resultante = crear_partida("grupal");
                    agregar_usuario(partida_resultante->jugadores, usuario_actual);
                    agregar_partida(partidas_grupales, partida_resultante);
                    mandar_mensaje(
                            usuario_actual->socket_id,
                            "\n\nSe ha creado una nueva partida grupal, debes esperar que otras 2 personas se unan a la"
                            "partida para poder iniciar. \n\n"

                    );
                    partida_resultante->iniciada = 0;
                }
            }
            else{
                partida_resultante = crear_partida("grupal");
                agregar_usuario(partida_resultante->jugadores, usuario_actual);
                agregar_partida(partidas_grupales, partida_resultante);
                partida_resultante->iniciada = 0;
                mandar_mensaje(
                        usuario_actual->socket_id,
                        "\n\nSe ha creado una nueva partida grupal, debes esperar que otras 2 personas se unan a la"
                        "partida para poder iniciar. \n\n"

                );
            }


        }
    }
    else{
        if(strcmp(parametros[0], "PARTIDA-INDIVIDUAL") == 0 | strcmp(parametros[0], "jugar") == 0) {
            partida_resultante = crear_partida("individual");
            lista_usuario_object = (ListaUsuarioObject *) malloc(sizeof(ListaUsuarioObject));
            partida_resultante->jugadores->cabeza = lista_usuario_object;
            partida_resultante->jugadores->cola = lista_usuario_object;
            partida_resultante->jugadores->tamano = 1;
            lista_usuario_object->usuario = usuario_actual;
            lista_usuario_object->anterior = NULL;
            lista_usuario_object->siguiente = NULL;
            lista_usuario_object->cabecera = partida_resultante->jugadores;
            agregar_partida(partidas_individuales, partida_resultante);
            partida_resultante->iniciada = 1;
            mandar_mensaje(
                    usuario_actual->socket_id,
                    "\n\nSe ha creado una nueva partida individual, puedes empezar el juego. \n\n"
            );
        } else if(strcmp(parametros[0],"PARTIDA-GRUPAL")==0){
            if(partidas_grupales->cola !=NULL){
                if(partidas_grupales->cola->partida_object->jugadores->tamano < 3){
                    partida_resultante = partidas_grupales->cola->partida_object;
                    agregar_usuario(partida_resultante->jugadores, usuario_actual);
                    sprintf(cadena_auxiliar,"\n\n Se ha unido el usuario: %s \n\n",usuario_actual->username);
                    broad_cast_mensaje(partida_resultante->jugadores,cadena_auxiliar,usuario_actual);

                    if(partidas_grupales->cola->partida_object->jugadores->tamano == 3){
                        broad_cast_mensaje(
                                partida_resultante->jugadores, "\n\n El juego puede empezar. \n\n", NULL
                        );
                        partida_resultante->iniciada = 1;
                    } else{
                        partida_resultante->iniciada = 0;
                        mandar_mensaje(usuario_actual->socket_id,"\n Debes esperar a una persona mas.");
                    }

                }else{
                    partida_resultante = crear_partida("grupal");
                    agregar_usuario(partida_resultante->jugadores, usuario_actual);
                    agregar_partida(partidas_grupales, partida_resultante);
                    mandar_mensaje(
                            usuario_actual->socket_id,
                            "\n\nSe ha creado una nueva partida grupal, debes esperar que otras 2 personas se unan a la"
                            "partida para poder iniciar. \n\n"

                    );
                    partida_resultante->iniciada = 0;
                }
            }
            else{
                partida_resultante = crear_partida("grupal");
                agregar_usuario(partida_resultante->jugadores, usuario_actual);
                agregar_partida(partidas_grupales, partida_resultante);
                partida_resultante->iniciada = 0;
                mandar_mensaje(
                        usuario_actual->socket_id,
                        "\n\nSe ha creado una nueva partida grupal, debes esperar que otras 2 personas se unan a la"
                        "partida para poder iniciar. \n\n"

                );
            }


        }
    }
    return  partida_resultante;
}





void protocolo_partida(Usuario *usuario_cliente, char **parametros){
    int numero_de_parametros = get_len(parametros);
    char *tablero_partida;
    if(numero_de_parametros >= 2){
        if(strcmp(parametros[1],"-s") == 0){
            tablero_partida = mostrar_partida(usuario_cliente->partida_actual);
            mandar_mensaje(usuario_cliente->socket_id, tablero_partida);
        }
    }
}


Usuario *protocolo_de_logueo(char **parametros_de_entrada, int *socket){
    char *username=NULL;
    char *password=NULL;
    char **parametros;
    int *tamano_parametros= (int *) malloc(sizeof(int)*1);
    Usuario *usuario_resultante = NULL;
    *tamano_parametros = get_len(parametros_de_entrada);
    if(*tamano_parametros >= 3){
        if(strcmp(parametros_de_entrada[1],"-u")==0){
            username = parametros_de_entrada[2];
        }
    }
    while (1){
        if(username == NULL){
            parametros = hacer_peticion(socket,"$ Ingresar Usuario:");
            if(parametros == NULL) {
                return  NULL;
            }
            *tamano_parametros = get_len(parametros);
            if(*tamano_parametros >= 2){
                if(strcmp(parametros[0],"username")){
                    username = parametros[1];
                }
            } else{
                username = parametros[0];
            }
        }else if(password == NULL) {
            parametros = hacer_peticion(socket, "$ -h * Ingrese el password:");
            if(parametros == NULL) {
                return  NULL;
            }
            *tamano_parametros = get_len(parametros);
            if (*tamano_parametros >= 2) {
                if (strcmp(parametros[0], "password")) {
                    password = parametros[1];
                }
            } else if (*tamano_parametros == 1) {
                password = parametros[0];
            }

        } else{
            break;
        }
    }
    usuario_resultante = loguear_usuario(conector_global, username, password);
    if(usuario_resultante != NULL) {
        usuario_resultante->socket_id = socket;
        char *mensaje = (char *)malloc(sizeof(char)*500);
        sprintf(mensaje,"-u %s",username);
        mandar_mensaje(socket,mensaje);
        sprintf(mensaje,"Se ha loguedo con exito %s", username);
        mandar_mensaje(socket,mensaje);
    }
    return usuario_resultante;
}


/*
 printf("Lista de comandos:\n");
	printf("REGISTRO -u (usuario) -p (contrasena)\n");
	printf("USUARIO -u <usuario>\n");
	printf("PARTIDA-INDIVIDUAL\n");
	printf("PARTIDA-GRUPAL\n");
	printf("VOCAL (vocal)\n");
	printf("CONSONANTE (consonante)\n");
	printf("SALIR\n");
	printf("RESOLVER (frase)\n\n");
 * */




void protocolos_server(int *socket, struct sockaddr_in from){

    char **parametros;
    Usuario *usuario_session=NULL;
    char *cadena_envio= (char *)malloc(sizeof(char)*1024);
    while(1) {
        sprintf(cadena_envio,"");

        parametros = leer_comandos(socket, &from);
        if(parametros != NULL) {


            if (strcmp(parametros[0], "loguear") == 0 | strcmp(parametros[0], "USUARIO") == 0) {
                usuario_session = protocolo_de_logueo(parametros, socket);
                if(usuario_session == NULL){
                    mandar_mensaje(socket,"\nIntento de logueo fallido");
                }
            } else if (strcmp(parametros[0], "desloguear") == 0) {
                if(usuario_session != NULL) {
                    free_user_object(usuario_session);
                    mandar_mensaje(socket,"\nSe ha deslogueado con exito.\n");
                } else{
                    mandar_mensaje(socket,"\nNo esta logueado.");
                }
            } else if (strcmp(parametros[0], "registrar") == 0 | strcmp(parametros[0], "REGISTRO") == 0) {
                protocolo_de_registro(parametros, socket);
            } else if (strcmp(parametros[0], "jugar") == 0 | strcmp(parametros[0], "PARTIDA-INDIVIDUAL") == 0 | strcmp(parametros[0], "PARTIDA-GRUPAL") == 0) {
                if(usuario_session != NULL){
                    if(usuario_session->partida_actual == NULL){
                        usuario_session->partida_actual = protocolo_iniciar_partida(parametros,usuario_session);

                    }
                    else{
                        mandar_mensaje(usuario_session->socket_id,"\n Usted ya esta en un partida.");
                    }
                } else{
                    mandar_mensaje(socket,"\n No se ha iniciado sesion previamente.");
                }
            } else if (strcmp(parametros[0], "partida") == 0) {
                if(usuario_session != NULL){
                    if(usuario_session->partida_actual != NULL){
                        protocolo_partida(usuario_session, parametros);
                    }
                    else{
                        mandar_mensaje(socket,"\n No se encuentra en ninguna partida.");
                    }
                }else{
                    mandar_mensaje(socket,"\n No se ha iniciado sesion previamente.");
                }
            }else if (strcmp(parametros[0], "consonante") == 0 | strcmp(parametros[0], "CONSONANTE") == 0) {
                if(usuario_session != NULL) {
                    if (usuario_session->partida_actual != NULL) {
                        if (usuario_session->partida_actual->iniciada) {
                            if(es_mi_turno(usuario_session)) {
                                if (strcmp(usuario_session->partida_actual->tipo_partida, "individual") == 0) {
                                    protocolo_consonante_individual(usuario_session, parametros);
                                } else if (strcmp(usuario_session->partida_actual->tipo_partida, "grupal") == 0) {
                                    protocolo_consonante_grupal(usuario_session, parametros);
                                }
                            }else{
                                mandar_mensaje(usuario_session->socket_id,"\n\nNo es tu turno.\n\n");
                            }
                        } else {
                            mandar_mensaje(socket, "\n La partida no ha iniciado. \n");
                        };
                    }
                    else {
                        mandar_mensaje(socket, "\n No se encuentra en ninguna partida.");
                    }
                }else{
                    mandar_mensaje(socket, "\n No se ha iniciado sesion previamente.");
                }

            } else if (strcmp(parametros[0], "vocal") == 0 | strcmp(parametros[0], "VOCAL") == 0) {
                if(usuario_session != NULL) {
                    if (usuario_session->partida_actual != NULL) {
                        if (usuario_session->partida_actual->iniciada) {
                            if(es_mi_turno(usuario_session)) {
                                if (strcmp(usuario_session->partida_actual->tipo_partida,"individual") == 0) {
                                    protocolo_vocal_individual(usuario_session, parametros);
                                } else if (strcmp(usuario_session->partida_actual->tipo_partida, "grupal") == 0) {
                                    protocolo_vocal_grupal(usuario_session, parametros);
                                }
                            }else{
                                mandar_mensaje(usuario_session->socket_id,"\n\nNo es su turno.\n\n");
                            }
                        } else {
                            mandar_mensaje(socket, "\n La partida no ah iniciado. \n");
                        };
                    } else {
                        mandar_mensaje(socket, "\n No se encuentra en ninguna partida.");
                    }
                }else{
                    mandar_mensaje(socket, "\n No se ha iniciado sesion previamente.");
                }

            } else if (strcmp(parametros[0], "adivinar") == 0 | strcmp(parametros[0], "RESOLVER") == 0) {
                if(usuario_session != NULL) {
                    if (usuario_session->partida_actual != NULL) {
                        if (usuario_session->partida_actual->iniciada) {
                            if(es_mi_turno(usuario_session)) {
                                if (strcmp(usuario_session->partida_actual->tipo_partida,
                                           "individual") == 0) {
                                    protocolo_adivinar_individual(usuario_session, parametros);
                                } else if (strcmp(usuario_session->partida_actual->tipo_partida,"grupal") == 0) {
                                    protocolo_adivinar_grupal(usuario_session, parametros);
                                }
                            }else{
                                mandar_mensaje(usuario_session->socket_id,"\n\nNo es su turno.\n\n");
                            }
                        } else {
                            mandar_mensaje(socket, "\n La partida no ah iniciado. \n");
                        }
                    }
                    else {
                        mandar_mensaje(socket, "\n No se encuentra en ninguna partida.");
                    }
                }else{
                    mandar_mensaje(socket, "\n No se ha iniciado sesion previamente.");
                }
            //} else if (strcmp(parametros[0], "cookie") == 0) {

            } else if (strcmp(parametros[0], "exit") == 0) {
                if(terminar_proceso_usuario(socket)){
                    break;
                }
            }else {
                mandar_mensaje(socket, "\nComando invalido\n");
            }

            if(usuario_session != NULL){
                if(usuario_session->partida_actual != NULL){
                    if(usuario_session->partida_actual->ganador != NULL){
                        terminar_partida(usuario_session);
                    }
                }
            }

        }else{
            if(usuario_session != NULL){
                free_user_object(usuario_session);
            }
            break;
        }
    }
}