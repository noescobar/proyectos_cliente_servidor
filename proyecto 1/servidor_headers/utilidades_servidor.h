#include <ctype.h>
#include "servidor_bd.h"

PGconn *conector_global;
ListaPartidas *partidas_individuales;
ListaPartidas *partidas_grupales;
ListaUsuarios *usuarios_logueados;


void mandar_mensaje(int *socket, const char *mensaje){
    char *buf = (char *)malloc(sizeof(char)*1024);
    sprintf(buf,"%s",mensaje);


    if (send(*socket, buf,1024, 0) <0 ){
        perror("sending stream message");
    }
}



void reusePort(int s)
{
    int one=1;

    if ( setsockopt(s,SOL_SOCKET,SO_REUSEADDR,(char *) &one,sizeof(one)) ==
         -1 )
    {
        printf("error in setsockopt,SO_REUSEPORT \n");
        exit(-1);
    }
}


int contruir_servidor(struct sockaddr_in from, int fromlen, char *argv[], int argc){
    int sd, psd;
    struct sockaddr_in server;
    struct hostent *hp, *gethostbyname();
    struct servent *sp;
    int length;
    char ThisHost[80];
    char *port;

     if(argc <2){
        printf("\nInstrucciones de uso: %s  <puerto servidor>", argv[0]);
        printf("\nSe utilizara el puerto por defecto.");
        port = DEFAULT_PORT;
    } else{
        port = argv[1];
    }


    sp = getservbyname("echo", "tcp");
/* get EchoServer Host information, NAME and INET ADDRESS */

    gethostname(ThisHost, MAXHOSTNAME);

/* OR strcpy(ThisHost,"localhost"); */

    printf("----TCP/Server running at host NAME: %s\n", ThisHost);
    if ( (hp = gethostbyname(ThisHost)) == NULL ) {
        fprintf(stderr, "Can't find host %s\n", port);
        exit(-1);
    }
    bcopy ( hp->h_addr, &(server.sin_addr),(socklen_t) hp->h_length);
    printf(" (TCP/Server INET ADDRESS is: %s )\n",
           inet_ntoa(server.sin_addr));

    /* Construct name of socket to send to. */
    server.sin_family = (sa_family_t)hp->h_addrtype;
/*OR server.sin_family = AF_INET; */

    server.sin_addr.s_addr = htonl(INADDR_ANY);

    /* server.sin_port = htons((u_short) 0); i*/
/*OR server.sin_port = sp->s_port; */
    server.sin_port = htons( (u_short) atoi(port));

/* Create socket on which to send and receive */

    /* sd = socket (PF_INET,SOCK_DGRAM,0); */
    sd = socket (hp->h_addrtype,SOCK_STREAM,0);

    if (sd<0) {
        perror("opening stream socket");
        exit(-1);
    }

    reusePort(sd);

    if ( bind( sd,(struct sockaddr *) &server, sizeof(server) ) ) {
        close(sd);
        perror("binding name to stream socket");
        exit(-1);
    }

    length = sizeof(server);
    if ( getsockname (sd,(struct sockaddr *)&server, (socklen_t *) &length) ) {
        perror("getting socket name");
        exit(0);
    }

    printf("\n El puerto del servidor es: %d\n", ntohs(server.sin_port));
    listen(sd,2);
    //fromlen = sizeof(from);
    return  sd;
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                                                     *
 * funcion **leer_comandos :                                                                                           *
 *                                                                                                                     *
 *      Descripcion : Esta funcion permite recibir un cadena de caracteres (ya sea atraves del socket) y convertirla en*
 *                    un vector de strings para su tilizacion en el servidor                                           *
 *                                                                                                                     *
 *      Parametros :                                                                                                   *
 *                                                                                                                     *
 *          *socket : Entero que me permite hacer la utilizacion del socket correspondiente, es utilizado para leer    *
 *                    y mirar lo que se halla mandado al servidor                                                      *
 *                                                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*/

char **leer_comandos(int *socket, struct sockaddr_in *from){
    char *cadena_comando=(char *) malloc(sizeof(char)*1024);
    char **parametros;
    int *i = (int*)malloc(sizeof(int)*1);
    *i = 0;
    int rc;
    if( (rc=(int)read(*socket,cadena_comando, 1024)) < 0) {
        return NULL;
    }
    if (rc > 0 ){
        if(cadena_comando[0] != '\0') {
            cadena_comando[rc] = ' ';
        }
    }
    else {
        if(from != NULL) {
            printf("\nEl cliente %s:%d se ha desconectado.", inet_ntoa((*from).sin_addr), ntohs((*from).sin_port));
        }
        close (*socket);
        free(cadena_comando);
        cadena_comando = NULL;
    }
    if(cadena_comando != NULL) {
        while(cadena_comando[*i]){
            if(cadena_comando[*i]=='\n'){
                cadena_comando[*i]= '\0';
            }
            cadena_comando[*i] = (char) tolower(cadena_comando[*i]);
            (*i)++;
        }
        parametros = split(cadena_comando, ' ');

        free(cadena_comando);
        return parametros;
    }else{
        return NULL;
    }
}



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                                                     *
 *                                                                                                                     *
 *                                                                                                                     *
 *                                                                                                                     *
 *                                                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*/

char **hacer_peticion(int *socket, const char *mensaje){

    mandar_mensaje(socket,mensaje);

    return  leer_comandos(socket,NULL);
}




/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                                                     *
 *                                                                                                                     *
 *                                                                                                                     *
 *                                                                                                                     *
 *                                                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*/

void broad_cast_mensaje(ListaUsuarios *lista_usuarios, const char *mensaje, Usuario *usuario){
    ListaUsuarioObject *lista_object_auxiliar = lista_usuarios->cabeza;
    while(lista_object_auxiliar != NULL){

        if(lista_object_auxiliar->usuario != usuario) {
            mandar_mensaje(lista_object_auxiliar->usuario->socket_id, mensaje);
        }
        lista_object_auxiliar = lista_object_auxiliar->siguiente;
    };
}



const int terminar_proceso_usuario(int *socket){
    mandar_mensaje(socket,"\nSe ha terminado la conexion con el servidor.\n\n");
    return 1;
}

void free_user_object(Usuario *usuario_session){
    char *cadena_envio= (char *)malloc(sizeof(char)*1024);
    sprintf(cadena_envio,"");
    if (usuario_session != NULL) {
        if(usuario_session->partida_actual != NULL){
            sprintf(cadena_envio,"\n El usuario %s ha abandonado la partida.", usuario_session->username);
            broad_cast_mensaje(usuario_session->partida_actual->jugadores,cadena_envio,usuario_session);
            remover_usuario(usuario_session->partida_actual->jugadores,usuario_session);
            usuario_session->partida_actual = NULL;
        }
        mandar_mensaje(usuario_session->socket_id,"-u");
        usuario_session->socket_id=NULL;
        //free(usuario_session);
        usuario_session = NULL;
    }
    free(cadena_envio);
}


int comprobar_partida(Usuario *usuario, const char *frase_adivinar){
    Partida *partida = usuario->partida_actual;
    char *frase = partida->frase;
    int *i = (int *) malloc(sizeof(int));
    int *correcto = (int *) malloc(sizeof(int));
    char *letra_auxiliar = (char *)malloc(sizeof(char)*1);
    *correcto = 1;
    if(frase_adivinar != NULL){
        if(strcmp(frase, frase_adivinar)){
            *correcto = 0;
        } else{
            for (*i = 0; *i < strlen(frase); *i = *i + 1) {
                sprintf(letra_auxiliar,"%c",frase[*i]);
                if (*letra_auxiliar != ' ') {
                    if (strstr(partida->letras_acertadas, letra_auxiliar) == NULL) {
                        sprintf(partida->letras_acertadas,"%s%c",partida->letras_acertadas,frase[*i]);
                    }
                }
            };
        }
    }
    else {
        for (*i = 0; *i < strlen(frase); *i = *i + 1) {
            *letra_auxiliar = frase[*i];
            if (*letra_auxiliar != ' ') {
                if (strstr(partida->letras_acertadas, letra_auxiliar) == NULL) {
                    *correcto = 0;
                    *i = (int) strlen(frase);
                }
            }
        };
    }
    if(*correcto == 1) {
        partida->ganador = usuario;
    }
    return *correcto;
}



int es_mi_turno(Usuario *usuario_actual){
    Partida *partida_actual = usuario_actual->partida_actual;
    ListaUsuarioObject *usuaro_obtejo = partida_actual->jugadores->cabeza;
    int es_el_turno = 0;
    int *i = (int *)malloc(sizeof(int)*1);
    *i = 0;

    while (usuaro_obtejo!=NULL){
        if(usuaro_obtejo->usuario == usuario_actual){
            if(partida_actual->turno == *i){
                es_el_turno = 1;
            }
            break;
        }
        usuaro_obtejo = usuaro_obtejo->siguiente;
        *i = *i +1;
    }
    free(i);
    return  es_el_turno;
}

Usuario *obtener_usuario_turno(Partida *partida){
    ListaUsuarioObject *usuario_object_aux = partida->jugadores->cabeza;
    Usuario *usuario_obtenido = NULL;
    int *i = (int*) malloc(sizeof(int)*1);
    *i = 0;
    while(usuario_object_aux != NULL){
        if(*i == partida->turno){
            usuario_obtenido = usuario_object_aux->usuario;
            break;
        }
        usuario_object_aux = usuario_object_aux->siguiente;
        *i = *i +1;
    }
    free(i);
    return usuario_obtenido;
}


char *mostrar_partida(Partida *partida_actual){
    Partida *partida = partida_actual;
    ListaUsuarioObject *jugadores_aux;
    int *i = (int *)malloc(sizeof(int)*1);
    char *cadena= (char*) malloc(sizeof(char)*1024);
    char *frase = partida->frase;
    char *letra_auxiliar = (char *) malloc(sizeof(char)*1);
    sprintf(letra_auxiliar,"");
    sprintf(cadena,"\n Partida actual :\n\n");
    for(*i=0; *i < strlen(frase); *i = *i +1){

        sprintf(letra_auxiliar,"%c",frase[*i]);
        if(*letra_auxiliar != ' ') {
            if (strstr(partida->letras_acertadas, letra_auxiliar) != NULL) {
                sprintf(cadena, "%s%s",cadena, letra_auxiliar);
            } else {
                sprintf(cadena,"%s_",cadena);
            }
        }else{
            sprintf(cadena,"%s ",cadena);
        }
    };
    sprintf(cadena,"%s\n\n",cadena);
    if(obtener_usuario_turno(partida) != NULL) {
        sprintf(cadena, "%sTurno del jugador : %s\n", cadena, obtener_usuario_turno(partida)->username);
    }
    sprintf(cadena,"%snumero de usuario en la partida: %i\n\n",cadena, partida_actual->jugadores->tamano);
    sprintf(cadena,"%spuntajes:",cadena);
    jugadores_aux = partida->jugadores->cabeza;
    while (jugadores_aux != NULL){
        sprintf(cadena,"%s\n\t%s: %d",cadena, jugadores_aux->usuario->username, *jugadores_aux->usuario->puntaje_actual);
        if(jugadores_aux->siguiente != NULL){
            sprintf(cadena,"%s, ",cadena);
        }
        jugadores_aux = jugadores_aux->siguiente;
    }
    sprintf(cadena,"%s\nusuario en esta partida:",cadena);
    jugadores_aux = partida->jugadores->cabeza;
    while (jugadores_aux != NULL){
        sprintf(cadena,"%s %s",cadena, jugadores_aux->usuario->username);
        if(jugadores_aux->siguiente != NULL){
            sprintf(cadena,"%s, ",cadena);
        }
        jugadores_aux = jugadores_aux->siguiente;
    }
    return cadena;
}


void aumentar_puntaje_usuario(Usuario *usuario_actual){
    if(*usuario_actual->puntaje_actual <=0){
        *usuario_actual->puntaje_actual = 0;
    }
    if(*usuario_actual->intentos <= 5){
        *usuario_actual->puntaje_actual = *usuario_actual->puntaje_actual + 150;
    }else if(*usuario_actual->intentos <= 8){
        *usuario_actual->puntaje_actual = *usuario_actual->puntaje_actual + 100;
    }else if(*usuario_actual->intentos <= 9){
        *usuario_actual->puntaje_actual = *usuario_actual->puntaje_actual + 70;
    }else if(*usuario_actual->intentos <= 12){
        *usuario_actual->puntaje_actual = *usuario_actual->puntaje_actual + 50;
    }else if(*usuario_actual->intentos <= 15){
        *usuario_actual->puntaje_actual = *usuario_actual->puntaje_actual + 0;
    }
}


void pasar_turno(Partida *partida_actual){
    partida_actual->turno =  partida_actual->turno + 1;
    if(partida_actual->turno >= partida_actual->jugadores->tamano){
        partida_actual->turno = 0;
    }
}

Usuario *get_usuario_ganador(ListaUsuarios *lista_de_usuarios, Usuario *usuario_excluir){
    ListaUsuarioObject *objeto_auxiliar = lista_de_usuarios->cabeza;
    Usuario *ganador = NULL;
    while(objeto_auxiliar != NULL){
        if (objeto_auxiliar->usuario != usuario_excluir) {
            if (ganador == NULL) {
                ganador = objeto_auxiliar->usuario;
            }
            if (ganador->puntaje_actual < objeto_auxiliar->usuario->puntaje_actual) {
                ganador = objeto_auxiliar->usuario;
            }
            objeto_auxiliar = objeto_auxiliar->siguiente;
        }
    }
    return  ganador;
}


const int es_vocal(char *letra){
    if( strstr(letra,"a") != NULL |strstr(letra,"e") != NULL |strstr(letra,"i") != NULL |strstr(letra,"o") != NULL |
        strstr(letra,"u") != NULL ){
        return  1;
    } else{
        return 0;
    }
}

Partida *crear_partida(const char *tipo_partida){
    char **parametros_frase = obtener_frase_partida(conector_global);
    Partida *partida_creada =  (Partida *) malloc(sizeof(Partida));
    partida_creada->id = crear_partida_bd(conector_global, atoi(parametros_frase[0]), tipo_partida);
    partida_creada->frase = parametros_frase[1];
    partida_creada->nombre_partida = NULL;
    partida_creada->jugadores = (ListaUsuarios *) malloc(sizeof(ListaUsuarios));
    partida_creada->jugadores->cabeza = NULL;
    partida_creada->jugadores->cola = NULL;
    partida_creada->jugadores->tamano = 0;
    partida_creada->ganador = NULL;
    partida_creada->turno = 0;
    pthread_mutex_init(&partida_creada->mutex_finalizar,NULL);
    partida_creada->tipo_partida = (char *) malloc(sizeof(char)*15);
    sprintf(partida_creada->tipo_partida,tipo_partida);
    return  partida_creada;
}


void terminar_partida(Usuario *usuario_actual){
    Partida *partida_actual = usuario_actual->partida_actual;
    pthread_mutex_lock (&partida_actual->mutex_finalizar);
    usuario_actual->partida_actual = NULL;
    remover_usuario(partida_actual->jugadores, usuario_actual);
    if(partida_actual->jugadores->tamano <= 0){
        if(partida_actual->ganador != NULL) {
            poner_ganador_partida(conector_global, *partida_actual->id, *partida_actual->ganador->id);
        }
        if(strcmp(partida_actual->tipo_partida,"individual")){
            remover_partida(partidas_individuales, partida_actual);
        }else if(strcmp(partida_actual->tipo_partida,"grupal")){
            remover_partida(partidas_grupales, partida_actual);
        }
        partida_actual->ganador = NULL;

        free(partida_actual);
        mandar_mensaje(usuario_actual->socket_id,"La partida ha finalizado");
    }
    pthread_mutex_unlock (&partida_actual->mutex_finalizar);
}