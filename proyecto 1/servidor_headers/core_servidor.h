#include <sys/time.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <string.h>
#include <pthread.h>
#include <libpq-fe.h>
#include "estructuras_servidor.h"

#define MAXHOSTNAME 80

#define DEFAULT_HOST "127.0.0.1";
#define DEFAULT_PORT "5000";

#define USER_PG "postgres"
#define PASSWORD_PG "pswd123"
#define DATA_BASE_PG "ruleta"
#define PORT_PG "5432"
#define HOST_PG "127.0.0.1"



/*  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
 *                                                                                                                     *
 *  funcion **split:                                                                                                   *
 *                                                                                                                     *
 *      descripcion: Esta funcion toma un arreglo de caracteres que podemos llamar string y lo parte en un vector de   *
 *                   strings partiendolo apartir de un  caracter que nosotros mismo definimos(para mas ejemplos se     *
 *                   puede tomar como referencia la funcion split de python).                                          *
 *                                                                                                                     *
 *      parametros :                                                                                                   *
 *          *string : Este es el arreglo de caracteres que queromos partir apartir de un simbolo                       *
 *          sep     : Este es el simbolo con el cual tomaremos como referencia para partir la cadena.                  *
 *                                                                                                                     *
 *      retorno : El resultado de esta funcion es un vector de cadena de caracteres que en cada posicion tendra los    *
 *                strings resultantes de separar la cadena principal.                                                  *
 *                                                                                                                     *
 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
*/

char **split ( char *string, const char sep) {

    char **lista;
    char *p = string;
    int i = 0;

    int pos;
    const int len = (int)strlen (string);

    lista = (char **) malloc (sizeof (char *));
    if (lista == NULL) { /* Cannot allocate memory */
        return NULL;
    }

    lista[pos=0] = NULL;

    while (i < len) {

        while ((p[i] == sep) && (i < len))
            i++;

        if (i < len) {

            char **tmp = (char **) realloc (lista , (pos + 2) * sizeof (char *));
            if (tmp == NULL) { /* Cannot allocate memory */
                free (lista);
                return NULL;
            }
            lista = tmp;
            //tmp = NULL;

            lista[pos + 1] = NULL;
            lista[pos] = (char *) malloc (sizeof (char));
            if (lista[pos] == NULL) { /* Cannot allocate memory */
                for (i = 0; i < pos; i++)
                    free (lista[i]);
                free (lista);
                return NULL;
            }

            int j = 0;
            for (i; ((p[i] != sep) && (i < len)); i++) {
                lista[pos][j] = p[i];
                j++;

                char *tmp2 = (char *) realloc (lista[pos],(j + 1) * sizeof (char));
                if (lista[pos] == NULL) { /* Cannot allocate memory */
                    for (i = 0; i < pos; i++)
                        free (lista[i]);
                    free (lista);
                    return NULL;
                }
                lista[pos] = tmp2;
                //tmp2 = NULL;
            }
            lista[pos][j] = '\0';
            pos++;
        }
    }

    return lista;
}

/*  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
 *                                                                                                                     *
 *  funcion get_len :                                                                                                  *
 *                                                                                                                     *
 *      descripcion: Esta funcion me permite obtener el tamano de un arreglo(especificamente de strings) se creo con el*
 *                   el objtivo de saber el tamano de uno para que se pudiera recorrer, de forma adecuada.             *
 *                                                                                                                     *
 *      parametros :                                                                                                   *
 *          **x : Este es el arreglo de strings al cual queremos calcularle el tamano.                                 *
 *                                                                                                                     *
 *      retorno : Entero i siendo i el resultado con el tamano del  arreglo                                            *
 *                                                                                                                     *
 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
*/

int get_len(char **x){ int i=0; while(x[i] != NULL){ i++;}return i;}

