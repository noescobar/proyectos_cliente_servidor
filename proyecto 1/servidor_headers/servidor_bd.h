#include "core_servidor.h"


/*
 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
 *  funcion establecer_conexion                                                                                        *
 *                                                                                                                     *
 *      descripcion: Esta funcion permite establecer una conexion directa con la base de datos haciendo uso de las     *
 *                   variables globales definidas al principio del codigo que describen la conexion con la base de     *
 *                   datos postgres.                                                                                   *
 *      retorno    : Se retorna un puntero que apunta directamente al conector de la base de datos y que permitira     *
 *                   manipularla desde el codigo c.                                                                    *
 *                                                                                                                     *
 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
*/

PGconn *establecer_conexion(){
    PGconn *conn;

    conn = PQsetdbLogin(HOST_PG,PORT_PG,NULL,NULL,DATA_BASE_PG, USER_PG, PASSWORD_PG);

    if (PQstatus(conn) != CONNECTION_BAD)
    {
        return conn;
    }else{
        printf("\n No se a podido establecer conexion con la base de datos.\n");
        exit(-1);
    }
}


/*
 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
 *  funcion get_all_user:                                                                                              *
 *                                                                                                                     *
 *      descripcion: Esta funcion me permite crear una lista de usuario con los elementos optenidos en la ejecucion de *
 *                   la consulta en la base de datos, la cual se datalal mas abajo.                                    *
 *                                                                                                                     *
 *      parametros:                                                                                                    *
 *          *conector: Este  es  un puntero al conector de la base de datos, que como se puede ver abajo debe de tener *
 *                     una conexion correta para que este se pueda utilizar de lo contrario no se hara nada.           *
 *                                                                                                                     *
 *      retorno:                                                                                                       *
 *          *usuarios_ontenidos: Esta  variable  esta  construida  con  la  estructura ListaUsuarios, para permitirnos *
 *                               manejar de forma de mas alto nivel las lista de los usuarios.                         *
 *                                                                                                                     *
 *      nota: Esta funcion  esta construida fundamentamente con la intencion de mostrar cual sera el esquema en el que *
 *            se  manipulara  la base de datos y la forma en al que seran manipuladas las estructuras de datos creadas *
 *            apartir de la abase de datos.                                                                            *
 *                                                                                                                     *
 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
*/

ListaUsuarios *get_all_user(PGconn *conector){
    PGresult *res;
    int *i = (int*)malloc(sizeof(int)*1);
    int *j = (int*)malloc(sizeof(int)*1);
    Usuario *aux_user;
    ListaUsuarios *usuarios_obtenidos=(ListaUsuarios*)malloc(sizeof(ListaUsuarios));

    /*  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * *
     *                                                                                                                 *
     * La siguiente sera una variable auxiliar que nos permitira contruir la lista de usuario moviendonos si perder la *
     * estructura  raiz  la cual es el apuntador usuarios_obtenidos, inicialmente se pondra a apuntar a la cabeza para *
     * iniciar la construccion en la cabeza de la lista.                                                               *
     *                                                                                                                 *
     *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * *
    */

    ListaUsuarioObject *aux_constructor_list=(ListaUsuarioObject *) malloc(sizeof(ListaUsuarioObject));

    aux_constructor_list->cabecera = usuarios_obtenidos;
    aux_constructor_list->anterior = NULL;
    aux_constructor_list->siguiente = NULL;
    aux_constructor_list->usuario = NULL;

    /*  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * *
     *                                                                                                                 *
     * Se inicializarn todas las variables de la estructura cabeza de la lista usuarios_obtenidos                      *
     * Como es el primer elemento de la lista no se tiene ni siguiente ni anterior.                                    *
     *                                                                                                                 *
     *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * *
    */

    usuarios_obtenidos->cabeza=NULL;
    usuarios_obtenidos->cola=NULL;
    usuarios_obtenidos->tamano=0;

    /* Como es la avariable cabeza se coloca a la cabeza a apuntar asi misma.*/

    usuarios_obtenidos->cabeza=aux_constructor_list;;

    if (PQstatus(conector) != CONNECTION_BAD) {

        /*  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * * *
         *                                                                                                             *
         * Para  ejecutar consultas directas a la base de datos postgres usando la libreria stdlib.h se hace uso de la *
         * funcion  PQexec  que  ejecuta  una consulta de codigo sql en el gestor de base de datos postgresql, hay que *
         * tener  cuenta  que el resultado de esta consulta sobre la base de datos arrojara como resultado una especie *
         * de  matriz  en  la  cual  sus  filas  seran  cada uno de los elementos de la tabla y sus columnas seran los *
         * atributos de la tabla o dependiendo de la consulta seran los valores del select.                            *
         *
         *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * */

        res = PQexec(conector, "select id, username from usuario");

        if (res != NULL && PGRES_TUPLES_OK == PQresultStatus(res))
        {
            for (*i = 0;*i < PQntuples(res); (*i)++)
            {
                if(*i != 0){

                    /*  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * *
                     *                                                                                                 *
                     *  La  siguiente  es  una forma de construir la lista de usuarios reservando los espacios de cada *
                     *  la forma en la que funciona es la siguiente:                                                   *
                     *      -Se  reservara  un  espacio  en memoria para el siguiente elemento de la lista ya que este *
                     *       puntero a un elemento siguiente estaba apuntando a NULL ya que anterioriormente no habian *
                     *       mas elementos en la lista, es por eso que se le reserva un espacio en memoria.            *
                     *      -Se  coloca  el  puntero anterior del elemento siguiente a apuntar al elemento actual para *
                     *       que se conserve la direccion de la lista de forma correcta.                               *
                     *      -Se  coloca  al  puntero  del  elemento  siguiente  de la lista a apuntar a NULL ya que el *
                     *       elemento  siguiente  es el ultimo de la lista por ende el puntero a un siguiente elemento *
                     *       despues de este al ser el ultimo deberia de ser NULL.                                     *
                     *      -Finalmente  se coloca a la lista auxiliar a apuntar al nuevo elemento para movernos en la *
                     *       lista y seguir construyendola.                                                            *
                     *                                                                                                 *
                     *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * *
                    */

                    aux_constructor_list->siguiente = (ListaUsuarioObject *)malloc(sizeof(ListaUsuarioObject));
                    aux_constructor_list->siguiente->anterior = aux_constructor_list;
                    aux_constructor_list->siguiente->siguiente = NULL;
                    aux_constructor_list->cabecera = aux_constructor_list->cabecera;
                    aux_constructor_list = aux_constructor_list->siguiente;
                }

                /*  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * * * *
                 *                                                                                                     *
                 *  Las  siguientes  lineas de codigo nos permiten crear un elemento usuario apartir de la informacion *
                 *  obtenida  de  la  base  de  datos  con la consulta realizada anteriormente, como el objetivo de la *
                 *  funcion  es  solamente  el de listar los usuario de la base de datos los valores partida y sockect *
                 *  seran  valores  invalidos  o que en resumen no serviran para ser utilizados de ninguna forma, solo *
                 *  para ser inicializados.                                                                            *
                 *                                                                                                     *
                 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * * * */

                aux_user = (Usuario *)malloc(sizeof(Usuario));
                aux_user->id = (int *)malloc(sizeof(int)*1);
                *aux_user->id = atoi(PQgetvalue(res,*i,0));
                sprintf(aux_user->username,"%s",PQgetvalue(res,*i,1));
                aux_user->partida_actual = NULL;
                aux_user->socket_id =(int *)malloc(sizeof(int)*1);;
                *aux_user->socket_id = -1;
                aux_constructor_list->usuario = aux_user;

            }
            PQclear(res);
            if(usuarios_obtenidos->cabeza->usuario == NULL){
                free(usuarios_obtenidos);
                usuarios_obtenidos = NULL;
            }
        } else{
            printf("\n No hay usuarios registrados.\n");
        }
    }else{
        printf("\n Se a perdido la conexion con la base de datos. \n");
        usuarios_obtenidos=NULL;
        free(aux_constructor_list);
    }
    free(i);
    free(j);
    return usuarios_obtenidos;
}





/*
 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
 *  funcion mostrar_usuario_list:                                                                                      *
 *                                                                                                                     *
 *      descripcion: Como se puede ver en el nombre esta funcion me permite mostrar los usuario contenidos en una n de *
 *                   estructura de  tipo ListaUsuarios, esta funcion me permite de forma simple comprobar los usuarios *
 *                   dentro de una lista.                                                                              *                                           *
 *                                                                                                                     *
 *      Parametros:                                                                                                    *
 *          lista: Esta  es  la  lista  que  contiene  a los usuario siendo esta la que recorreremos para mostrar cada *
 *                 contenido en esta misma.                                                                            *
 *                                                                                                                     *
 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
*/

int registrar_usuario(PGconn *conector, char *username,char *password){
    PGresult *res;
    int resultado=0;
    char *consulta= (char *)malloc(sizeof(char)*1024);

    if (PQstatus(conector) != CONNECTION_BAD) {
        sprintf(consulta, "INSERT INTO usuario (username,password) VALUES('%s','%s')", username, password);
        res = PQexec(conector, consulta);
        if (res != NULL && PGRES_COMMAND_OK == PQresultStatus(res)) {
            printf("\nSe ha registrado el usuario %s con exito", username);
            resultado = 1;
        }
    }

    free(consulta);

    return resultado;

}


/*
 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
 *
 *  funcion loguear_usuario:                                                                                           *
 *                                                                                                                     *
 *      Descripcion: Esta funcion permite vaidar el logueo de un usuario de forma que se sepa si el logue es realizado *
 *                   con exito o si este falla.                                                                        *
 *                                                                                                                     *
 *      Parametros:                                                                                                    *
 *                                                                                                                     *
 *          conector: El conector de la base de datos, debe ser valido y estar funcionando.                            *
 *          username: Esta es una cadena que contendra el nombre de usuario.                                           *
 *          password: Esta es una cadena que contendra el password del usuario.                                        *
 *                                                                                                                     *
 *      Retorno: Se  retorna  un  apuntar  a  una  estructura usuario en caso de que el logueo halla sido exito, de lo *
 *               contrario se devolvera un apuntador a un NULL lo que querra decir que el logueo a fallado.            *
 *                                                                                                                     *
 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
*/

Usuario *loguear_usuario(PGconn *conector, char *username,char *password){
    PGresult *res;
    Usuario *usuario_obtenido=NULL;
    char *consulta= (char *)malloc(sizeof(char)*1024);

    if (PQstatus(conector) != CONNECTION_BAD) {
        sprintf(consulta, "select id, username from usuario where username='%s' and password = '%s' ", username, password);
        res = PQexec(conector, consulta);
        if (res != NULL && PGRES_TUPLES_OK == PQresultStatus(res) && PQntuples(res)) {
            printf("\nEl logueo del usuario %s se a realizado con exito.\n", username);
            usuario_obtenido = (Usuario*) malloc(sizeof(Usuario)*1);
            usuario_obtenido->id = (int *) malloc(sizeof(int)*1);
            *usuario_obtenido->id = atoi(PQgetvalue(res,0,0));
            sprintf(usuario_obtenido->username,"%s",PQgetvalue(res,0,1));
            usuario_obtenido->partida_actual = NULL;
            usuario_obtenido->puntaje_actual = (int *) malloc(sizeof(int)*1);
            *usuario_obtenido->puntaje_actual = 0;
            usuario_obtenido->intentos = (int *) malloc(sizeof(int)*1);
            *usuario_obtenido->intentos = 0;
            usuario_obtenido->socket_id = (int *) malloc(sizeof(int)*1);
            *usuario_obtenido->socket_id = -1;
        }
    }

    free(consulta);

    return usuario_obtenido;

}

char *obtener_frase(PGconn *conector){
    char *frase_obtenida = (char *) malloc(sizeof(char)*500);
    PGresult *res;
    char *consulta= (char *)malloc(sizeof(char)*1024);

    if (PQstatus(conector) != CONNECTION_BAD) {
        sprintf(consulta, "select id, frase from frase order by usos limit 1");
        res = PQexec(conector, consulta);
        if (res != NULL && PGRES_TUPLES_OK == PQresultStatus(res) && PQntuples(res)) {
            sprintf(frase_obtenida,"%s",PQgetvalue(res,0,1));

        }
    }
    sprintf(frase_obtenida, "esta es una frase de prueba");
    return frase_obtenida;
}

char *obtener_frase_id(PGconn *conector){
    char *frase_obtenida = (char *) malloc(sizeof(char)*500);
    PGresult *res;
    char *consulta= (char *)malloc(sizeof(char)*1024);

    if (PQstatus(conector) != CONNECTION_BAD) {
        sprintf(consulta, "select id, frase from frase order by usos limit 1");
        res = PQexec(conector, consulta);
        if (res != NULL && PGRES_TUPLES_OK == PQresultStatus(res) && PQntuples(res)) {
            sprintf(frase_obtenida,"%s;%s",PQgetvalue(res,0,0),PQgetvalue(res,0,1));

        }
    }
    return frase_obtenida;
}

void aumentar_uso_frase(PGconn *conector, int id){
    PGresult *res;
    char *consulta= (char *)malloc(sizeof(char)*1024);

    if (PQstatus(conector) != CONNECTION_BAD) {
        sprintf(consulta, "UPDATE frase SET usos = usos + 1 WHERE  id = %d",id);
        res = PQexec(conector, consulta);
        if (res != NULL && PGRES_TUPLES_OK == PQresultStatus(res)) {

        }
    }

}


char **obtener_frase_partida(PGconn *conector){
    char **frase_id;
    char *resultado_consulta;
    resultado_consulta = obtener_frase_id(conector);
    frase_id = split(resultado_consulta, ';');
    aumentar_uso_frase(conector,atoi(frase_id[0]));
    return frase_id;

}

int *crear_partida_bd(PGconn *conector, int frase_id, const char *tipo_partida){
    PGresult *res;
    char *consulta= (char *)malloc(sizeof(char)*1024);
    int *id_partida = (int *)malloc(sizeof(int)*1);
    *id_partida = -1;
    if (PQstatus(conector) != CONNECTION_BAD) {
        sprintf(consulta,
                "with rows as (INSERT INTO partida (frase_id,tipo_partida) VALUES(%d,'%s') RETURNING id) SELECT "
                "id FROM rows",frase_id, tipo_partida);
         printf("\n esta es la consulta de partida --> %s \n",consulta);
        res = PQexec(conector, consulta);

            *id_partida = atoi(PQgetvalue(res,0,0));
            printf("\n id de la partida --> %s \n",PQgetvalue(res,0,0));

    }

    return id_partida;
}


void poner_ganador_partida(PGconn *conector, int partida_id, int usuario_id){
    PGresult *res;
    char *consulta= (char *)malloc(sizeof(char)*1024);
    if (PQstatus(conector) != CONNECTION_BAD) {
        sprintf(consulta,"UPDATE partida SET  usuario_ganador_id = %d WHERE id = %d", usuario_id, partida_id);
        printf("\n ganador -->'%s'\n", consulta);
        res = PQexec(conector, consulta);
        if (res != NULL && PGRES_COMMAND_OK == PQresultStatus(res)) {
        }
    }
    free(consulta);
}
