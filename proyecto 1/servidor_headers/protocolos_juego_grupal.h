#include "protocolos_juego_individual.h"


void protocolo_adivinar_grupal(Usuario *usuario, char **parametros){
    int *i = (int *)malloc(sizeof(int)*1);
    *i = 1;
    char *frase_parametro= (char *)malloc(sizeof(char)*1024);
    char *cadena_mensaje= (char *)malloc(sizeof(char)*1024);
    char *tablero;
    sprintf(frase_parametro,"");
    while(parametros[*i]!=NULL){
        sprintf(frase_parametro,"%s%s",frase_parametro,parametros[*i]);
        (*i)++;
        if(parametros[*i]!=NULL){
            sprintf(frase_parametro,"%s ",frase_parametro);
        }
    }
    if(comprobar_partida(usuario,frase_parametro)){
        *usuario->intentos = *usuario->intentos + 1;
        aumentar_puntaje_usuario(usuario);
        tablero = mostrar_partida(usuario->partida_actual);
        sprintf(cadena_mensaje,"\n\nHas adivinado la frase\n %s", tablero);
        mandar_mensaje(usuario->socket_id,cadena_mensaje);
        sprintf(cadena_mensaje,"\n El usuario %s ha adivinado la frase y ganado la partida.\n %s",usuario->username, tablero);
        broad_cast_mensaje(usuario->partida_actual->jugadores,cadena_mensaje,usuario);
    } else{
        *usuario->intentos = *usuario->intentos + 1;
        *usuario->puntaje_actual = 0;
        usuario->partida_actual->ganador = get_usuario_ganador(usuario->partida_actual->jugadores, usuario);
        tablero = mostrar_partida(usuario->partida_actual);
        sprintf(
                cadena_mensaje,
                "\n\nTe has equivocado en la frase y has perdido la partida, ha ganado %s\n %s",
                tablero, usuario->partida_actual->ganador->username
        );
        mandar_mensaje(usuario->socket_id,cadena_mensaje);
        sprintf(
                cadena_mensaje,
                "\n El usuario %s ha fallado al adivinar la frase y ha perdido la partida, ha ganado el usuario %s.\n %s",
                usuario->username,usuario->partida_actual->ganador->username, tablero
        );
        broad_cast_mensaje(usuario->partida_actual->jugadores,cadena_mensaje,usuario);
    }
    free(i);
    free(cadena_mensaje);
    free(frase_parametro);
}



void protocolo_vocal_grupal(Usuario *usuario, char **parametros){
    Partida *partida_actual = usuario->partida_actual;
    char *tablero;
    char *mensaje = (char *)malloc(sizeof(char)*1024);
    char *segundo_parametro = (char *)malloc(sizeof(char)*1);
    if(*usuario->puntaje_actual >= 50){
        if(parametros[1] != NULL){
            *usuario->puntaje_actual = *usuario->puntaje_actual - 50;
            sprintf(segundo_parametro,"%c",parametros[1][0]);
            *usuario->intentos = *usuario->intentos + 1;
            if(strstr(partida_actual->letras_jugadas, segundo_parametro) == NULL){
                if (strstr(partida_actual->frase, segundo_parametro) != NULL) {
                    sprintf(partida_actual->letras_acertadas,"%s%s",partida_actual->letras_acertadas, segundo_parametro);


                    if(comprobar_partida(usuario,NULL)) {
                        aumentar_puntaje_usuario(usuario);
                        tablero = mostrar_partida(partida_actual);
                        sprintf(mensaje,"\n Has acertado la vocal %s\n %s",segundo_parametro,tablero);
                        mandar_mensaje(usuario->socket_id,mensaje);
                        sprintf(
                                mensaje,
                                "\n El usuario %s ha acertado la vocal %s y ha ganado la partida\n %s",
                                usuario->username, segundo_parametro, tablero
                        );
                    }else{
                        tablero = mostrar_partida(partida_actual);
                        sprintf(mensaje,"\n Has acertado la vocal %s\n %s",segundo_parametro,tablero);
                        mandar_mensaje(usuario->socket_id,mensaje);
                        sprintf(
                                mensaje,
                                "\n El usuario %s ha acertado la vocal %s\n %s",
                                usuario->username, segundo_parametro, tablero
                        );
                    }
                    broad_cast_mensaje(partida_actual->jugadores,mensaje,usuario);

                } else{
                    pasar_turno(partida_actual);
                    tablero = mostrar_partida(partida_actual);
                    sprintf(mensaje,"\n Te has equivocado con la vocal %s pierdes turno\n %s",segundo_parametro,tablero);
                    mandar_mensaje(usuario->socket_id,mensaje);
                    sprintf(mensaje,"\n El usuario %s se equivocado con la vocal %s\n %s",usuario->username, segundo_parametro,tablero);
                    broad_cast_mensaje(partida_actual->jugadores,mensaje,usuario);
                }
            }else{
                tablero = mostrar_partida(partida_actual);
                sprintf(mensaje,"\nLa vocal %s ya ha sido utilizada %s",segundo_parametro,tablero);
                mandar_mensaje(usuario->socket_id,mensaje);
                sprintf(mensaje,"\n El usuario %s ha usado la vocal %s repetida\n %s",usuario->username, segundo_parametro,tablero);
                broad_cast_mensaje(partida_actual->jugadores,mensaje,usuario);
            }
        }
    } else{
        mandar_mensaje(usuario->socket_id,"\n\nNo puedes usar una vocal, no posees los puntos suficientes para usarla");
    }
}


void protocolo_consonante_grupal(Usuario *usuario, char **parametros){
    Partida *partida_actual = usuario->partida_actual;
    char *tablero;
    char *mensaje = (char *)malloc(sizeof(char)*1024);
    char *segundo_parametro = (char *)malloc(sizeof(char)*1);
    if(parametros[1] != NULL){
        if(es_vocal(parametros[1]) == 1){
            mandar_mensaje(
                    usuario->socket_id,"\nNo ingreso una consonante, valida."
            );
        }else {
            sprintf(segundo_parametro, "%c", parametros[1][0]);
            *usuario->intentos = *usuario->intentos + 1;
            if (strstr(partida_actual->letras_jugadas, segundo_parametro) == NULL) {
                sprintf(partida_actual->letras_jugadas, "%s%s", partida_actual->letras_jugadas, segundo_parametro);
                if (strstr(partida_actual->frase, segundo_parametro) != NULL) {
                    sprintf(partida_actual->letras_acertadas, "%s%s", partida_actual->letras_acertadas,
                            segundo_parametro);
                    if (comprobar_partida(usuario, NULL)) {
                        aumentar_puntaje_usuario(usuario);
                        tablero = mostrar_partida(partida_actual);
                        printf("\naqui\n");
                        sprintf(mensaje, "\nHas acertado la consonante %s, has ganado el juego\n %s", segundo_parametro,
                                tablero);
                        mandar_mensaje(usuario->socket_id, mensaje);
                        sprintf(
                                mensaje,
                                "\nEl usuario %s acertado la consonante %s y ha ganado el juego\n %s",
                                partida_actual->ganador->username, segundo_parametro, tablero
                        );
                        broad_cast_mensaje(partida_actual->jugadores, mensaje, usuario);
                    } else {
                        tablero = mostrar_partida(partida_actual);
                        printf("\nacertado\n");
                        sprintf(mensaje, "\nHas acertado la consonante %s\n %s", segundo_parametro, tablero);
                        mandar_mensaje(usuario->socket_id, mensaje);
                        sprintf(
                                mensaje,
                                "\nEl usuario %s acertado la consonante %s\n %s",
                                usuario->username, segundo_parametro, tablero
                        );
                        broad_cast_mensaje(partida_actual->jugadores, mensaje, usuario);
                    }
                } else {
                    pasar_turno(partida_actual);
                    tablero = mostrar_partida(partida_actual);
                    sprintf(mensaje, "\nTe has equivocado con la consonante %s pierdes turno\n %s", segundo_parametro,
                            tablero);
                    mandar_mensaje(usuario->socket_id, mensaje);
                    sprintf(mensaje, "\n El usuario %s se equivocado con la consonante %s\n %s", usuario->username,
                            segundo_parametro, tablero);
                    broad_cast_mensaje(partida_actual->jugadores, mensaje, usuario);
                }
            } else {
                pasar_turno(partida_actual);
                tablero = mostrar_partida(partida_actual);
                sprintf(mensaje, "\nLa consonante %s ya ha sido utilizada %s", segundo_parametro, tablero);
                mandar_mensaje(usuario->socket_id, mensaje);
                sprintf(mensaje, "\n El usuario %s ha usado la consonante %s repetida\n %s", usuario->username,
                        segundo_parametro, tablero);
                broad_cast_mensaje(partida_actual->jugadores, mensaje, usuario);
            }
        }
    }else{
        mandar_mensaje(usuario->socket_id,"\n\nNo ha proporporcionado ningun segundo parametro.\n\n");
    }
}
