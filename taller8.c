#include <stdio.h>
#include <malloc.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <wait.h>
#include <ctype.h>


/*
 * Taller 8:
 *
 * Hacer el juego del triqui, donde un proceso hijo reciba, la informacion de teclado y otro, va a analizar quien gana
 * el juego.
 *
 * */
int *crear_proceso(void (*funcion)()){
    int *proceso_hijo;
    proceso_hijo = (int*) malloc(sizeof(int));
    *proceso_hijo = fork();
    switch (*proceso_hijo){
        case -1:
            printf("\n Ha sucedido un error y no puede iniciarse el hilo");
            break;
        case 0:
            funcion();
            break;
        default:
            break;
    }
    return proceso_hijo;
}

struct Tablero{
    char *matriz[3];
    char *simbolo_jugador1;
    int *ultima_jugada_1;
    char *simbolo_jugador2;
    int *ultima_jugada_2;
    int dimensiones[2];
    int *terminado;
};
typedef struct Tablero Tablero;

static Tablero *tablero_actual;

void mostrar_tablero(Tablero *instancia){
    int *i;
    i = (int*)malloc(sizeof(int));
    int *j;
    j = (int*)malloc(sizeof(int));
    for(*i=0; *i < 3; (*i)++){
        for (*j = 0; *j < 3; (*j)++) {
            if(*j == 0){
                printf(" %c |", instancia->matriz[*j][*i]);
            } else{
                if(*j == 2){
                    printf("| %c ", instancia->matriz[*j][*i]);
                } else{
                    printf(" %c ", instancia->matriz[*j][*i]);
                }
            }
        }
        printf("\n");
        for (*j = 0; *j < 3; (*j)++) {
            if(*i != 2){
                if(*j == 0){
                    printf("---|");
                } else{
                    if(*j == 2){
                        printf("|---");
                    }else{
                        printf("---");
                    }
                }
            }
        }
        printf("\n");
    }
}

int *encontrar_coordenadas(int *posicion_entera,int *dimensiones_matriz){
    int *coordenadas = (int*)malloc(sizeof(int)*2);
    int *a = (int*)malloc(sizeof(int)*1);
    int *b = (int*)malloc(sizeof(int)*1);
    *a = *posicion_entera % dimensiones_matriz[0];
    if(*a==0){ *a = dimensiones_matriz[0];};
    *b = (*posicion_entera / dimensiones_matriz[1]);
    if(*posicion_entera % dimensiones_matriz[1] == 0){
        *b = (*posicion_entera / dimensiones_matriz[1])-1;
    }
    coordenadas[0] = *a - 1;
    coordenadas[1] = *b;
    return coordenadas;
}

void triqui(){
    char *simbolos_disponibles = (char*) malloc(sizeof(char)*2);
    char *input = (char*) malloc(sizeof(char)*1);
    int *running = (int*) malloc(sizeof(int)*1);
    *running = 1;
    simbolos_disponibles[0] = 'O';
    simbolos_disponibles[1] = 'X';
    printf("\n");
    printf(" ________________________________________________________ \n");
    printf("|                                                        | \n");
    printf("|             Bienvenido al triqui multiproceso          | \n");
    printf("|________________________________________________________| \n");
    printf("\n");
    printf("\n");
    mostrar_tablero(tablero_actual);
    printf("\n");
    printf("\n");
    while(*running == 1) {
        printf("Los posibles simbolos a escoger son %c y %c.\n", simbolos_disponibles[0], simbolos_disponibles[1]);
        printf("Porfavor seleccione una de las opciones : ");
        scanf("%c", tablero_actual->simbolo_jugador1);
        if (toupper(*tablero_actual->simbolo_jugador1) == toupper(simbolos_disponibles[0])) {
            *tablero_actual->simbolo_jugador1 = simbolos_disponibles[0];
            *tablero_actual->simbolo_jugador2 = simbolos_disponibles[1];
            *running = 0;
        } else {
            if(toupper(*tablero_actual->simbolo_jugador1) == toupper(simbolos_disponibles[1])) {
                *tablero_actual->simbolo_jugador1 = simbolos_disponibles[1];
                *tablero_actual->simbolo_jugador2 = simbolos_disponibles[0];
                *running = 0;
            }else{
                printf("\nNo ha elegido una de la opciones validas, porfavor elija una de las opciones. \n");
            }
        };
    }
    printf("\n");
    printf(" ________________________________________________________  \n");
    printf("|                   Manual de uso                        | \n");
    printf("|Para  poder jugar  este juego debe de escribir un numero| \n");
    printf("|entre  1  y  9 tratando de ubicarse en el juego como una| \n");
    printf("|unica linea para poder jugar correctamente, recuerde que| \n");
    printf("|que  el simbolo que se colocara en la posicion que usted| \n");
    printf("|elija  sera  el simbolo que acabo de elegir, que en este| \n");
    printf("|caso  seria  '%c'  toda posicion inconsistente se tomara | \n", *tablero_actual->simbolo_jugador1);
    printf("|como   la   posicion  1,  refiriendose  a  una  posicion| \n");
    printf("|inconsistente como cualquier numero negativo, letra o el| \n");
    printf("|numero cero.                                            | \n");
    printf("|________________________________________________________| \n");

    int *playing = (int*)malloc(sizeof(int)*1);
    *playing = 1;
    printf("\nUsted hara la primera jugada.");
    int *posicion_elegida = (int*)malloc(sizeof(int));
    int *coordenadas;
    while(*playing == 1){
        printf("\n");
        mostrar_tablero(tablero_actual);
        printf("\nPorfavor ingrese la posicion como se explico en el manual : ");
        scanf ("%s", input);
        if(isdigit(*input)){
            *posicion_elegida = atoi(input);
            if(*posicion_elegida <= 0){
                *posicion_elegida = 1;
            }
            printf("\n La posicion valida recibida a sido  %i \n", *posicion_elegida);
            coordenadas = encontrar_coordenadas(posicion_elegida, tablero_actual->dimensiones);
            printf("\n Las coordenadas serian [%i,%i].\n",  coordenadas[0], coordenadas[1]);
            tablero_actual->ultima_jugada_1 = coordenadas;
            while (tablero_actual->ultima_jugada_1 != NULL){
                sleep(1);
            }
            if(tablero_actual->matriz[coordenadas[0]][coordenadas[1]] == ' ') {
                tablero_actual->matriz[coordenadas[0]][coordenadas[1]] = *tablero_actual->simbolo_jugador1;
            }else{
                printf("\nLa posicion que ha elegido ya esta ocupada por un simbolo de una juagada anterior. \n");
            }


        }

    }

}


void proceso_1(){
    triqui();
}

void proceso_2(){
    int *coordenadas;
    while(*tablero_actual->terminado == 0){
        if(tablero_actual->ultima_jugada_1 != NULL){
            coordenadas = tablero_actual->ultima_jugada_1;
            if(tablero_actual->matriz[coordenadas[0]][coordenadas[1]] == ' ') {
                tablero_actual->matriz[coordenadas[0]][coordenadas[1]] = *tablero_actual->simbolo_jugador1;
            }else{
                printf("\nLa posicion que ha elegido ya esta ocupada por un simbolo de una juagada anterior. \n");
            };
        }
        sleep(1);
    }
}


int main() {
    // int *procesos_hijos;
    int *child_pid;
    int *status_child =  (int*) malloc(sizeof(int));
    int *child_pid_2;
    int *status_child_2 =  (int*) malloc(sizeof(int));
    int *terminado =  (int*) malloc(sizeof(int));
    *terminado =  0;
    tablero_actual = malloc(sizeof(struct Tablero));
    tablero_actual= mmap(NULL, sizeof *tablero_actual, PROT_READ | PROT_WRITE,
                         MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    int *i;
    i = (int*)malloc(sizeof(int));
    int *j;
    j = (int*)malloc(sizeof(int));
    for(*i=0; *i < 3; (*i)++){
        tablero_actual->matriz[*i] = (char*)malloc(sizeof(char)*3);
        for (*j = 0; *j < 3; (*j)++) {
            tablero_actual->matriz[*i][*j] = ' ';
        }
    }
    tablero_actual->simbolo_jugador1 = (char*)malloc(sizeof(char)*1);
    tablero_actual->simbolo_jugador2 = (char*)malloc(sizeof(char)*1);
    tablero_actual->dimensiones[0] = 3;
    tablero_actual->dimensiones[1] = 3;
    tablero_actual->terminado = terminado;

    child_pid = crear_proceso(&proceso_1);

    if(*child_pid != 0){

        child_pid_2 = crear_proceso(&proceso_2);
        waitpid(*child_pid_2, status_child_2, 0);
    }

    return  0;
}
