#include <stdio.h>
#include <math.h>
#include <malloc.h>

/*
 * Taller 1:
 *
 *
 *
 * */


float *suma(float *a,float  *b){
    float *c;
    c = NULL;
    c = (float*)malloc(sizeof(float));
    *c = *a + *b;
    return c;
}


float *resta(float *a,float  *b){
    float *c;
    c = NULL;
    c = (float*)malloc(sizeof(float));
    *c = *a - *b;
    return c;
}

float *multiplicacion(float *a,float  *b){
    float *c;
    c = NULL;
    c = (float*)malloc(sizeof(float));
    *c = *a * *b;
    return c;
}

float *division(float *a,float  *b){
    float *c;
    c = NULL;
    c = (float*)malloc(sizeof(float));
    *c = *a / *b;
    return c;
}


float *potenciacion(float *base, float *potencia){
    float *c;
    c = NULL;
    c = (float*)malloc(sizeof(float));
    *c = powf(*base, *potencia);
    return c;
}


float *logaritmacion(float *numero,float  *base){
    float *c;
    c = NULL;
    c = (float*)malloc(sizeof(float));
    *c = logf(*numero)/logf(*base);
    return c;
}

int main(){
    float *a;
    float *b;
    float *result;
    int *opcion;
    result= NULL;
    a = NULL;
    b = NULL;
    opcion = NULL;
    a = (float*)malloc(sizeof(float));
    b = (float*)malloc(sizeof(float));
    result = (float*)malloc(sizeof(float));
    opcion = (int*)malloc(sizeof(int));
    printf("0. Salir.\n");
    printf("1. Sumar dos numeros.\n");
    printf("2. Restar dos numeros.\n");
    printf("3. Multiplicacion entre dos numeros.\n");
    printf("4. Dividir dos numeros.\n");
    printf("5. Sacar potencia de un numero.\n");
    printf("6. Realizar el logritmo de dos numeros.\n");
    printf("\nElige una opcion para ser ejecutadas.:\n");
    scanf("%d",opcion);
    switch (*opcion){
        case 1:
            printf("Ingrese el primer numero:  \n");
            scanf("%f",a);
            printf("Ingrese el segundo numero: \n");
            scanf("%f",b);
            result =  suma(a, b);
            printf("El resultado es: %f\n", *result);
            break;
        case 2:
            printf("Ingrese el primer numero:  \n");
            scanf("%f",a);
            printf("Ingrese el segundo numero: \n");
            scanf("%f",b);
            result =  resta(a, b);
            printf("El resultado es: %f\n", *result);
            break;
        case 3:
            printf("Ingrese el primer numero:  \n");
            scanf("%f",a);
            printf("Ingrese el segundo numero: \n");
            scanf("%f",b);
            result =  multiplicacion(a, b);
            printf("El resultado es: %f\n", *result);
            break;
        case 4:
            printf("Ingrese el primer numero:  \n");
            scanf("%f", a);
            printf("Ingrese el segundo numero: \n");
            scanf("%f", b);
            result =  division(a,b);
            printf("El resultado es: %f\n", *result);
            break;
        case 5:
            printf("Ingrese el numero base:  \n");
            scanf("%f",a);
            printf("Ingrese el numero potencia: \n");
            scanf("%f",b);
            result =  potenciacion(a, b);
            printf("El resultado es: %f\n", *result);
            break;
        case 6:
            printf("Ingrese el numero base:  \n");
            scanf("%f", a);
            printf("Ingrese el numero: \n");
            scanf("%f", b);
            result =  logaritmacion(a, b);
            printf("El resultado es: %f\n", *result);
            break;
        default:
            break;
    }
    free(a);
    free(b);
    free(result);
    return  0;
}
