#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include <unistd.h>
#include <pthread.h>


/*
 * Taller 12:
 *
 * Crear un programa con 3 hilos donde cada uno realice las siguientes operaciones:
 *
 * hilo 1: Reciba un numero y devuelva si hace parte de la serie fibonacci.
 * hilo 2: Reciba un numero y devuelva si es primo.
 * hilo 2: Reciba dos numeros y devuelva el MCD y el MCM.
 *
 * */

struct MCD_MCM{
    int *MCD;
    int *MCM;
};

typedef  struct MCD_MCM MCD_MCM;

int *resultado_fibonacci;
int *resultado_primo;
MCD_MCM *resultado_mcd_mcm;

int *es_primo_i(int *numero){
    int *raiz = (int*) malloc(sizeof(int)*1);
    int *bool= (int*) malloc(sizeof(int)*1);
    int *j= (int*) malloc(sizeof(int)*1);
    *raiz =(int)sqrt((double)*numero);
    *bool = 1;
    for (*j = 2; *j <= *raiz; *j = *j + 1) {
        if(*numero % *j == 0){
            *bool = 0;
            break;
        }
    }
    free(raiz);
    free(j);
    return bool;
}

int *hace_parte_de_fibonacci(int *numero){
    int *bool = (int*)malloc(sizeof(int)*1);
    int *ultimo_numero_fibonacci= (int*)malloc(sizeof(int)*1);
    *ultimo_numero_fibonacci = 1;
    int *penultimo_numero_fibonacci= (int*)malloc(sizeof(int)*1);
    *penultimo_numero_fibonacci = 0;
    int *i = (int*)malloc(sizeof(int)*1);
    *bool = 0;
    for(*i = 0; *i <=*numero & *ultimo_numero_fibonacci <= *numero; (*i)++){
        *ultimo_numero_fibonacci = *ultimo_numero_fibonacci + *penultimo_numero_fibonacci;
        *penultimo_numero_fibonacci = *ultimo_numero_fibonacci - *penultimo_numero_fibonacci;
    }
    if(*numero == *ultimo_numero_fibonacci | *numero == *penultimo_numero_fibonacci){
        *bool = 1;
    }
    return bool;
}


int *hallar_mcd(int *numeros){
    int *variable_axuliar_1 = (int*) malloc(sizeof(int)*1);
    *variable_axuliar_1 = numeros[0];
    int *variable_axuliar_2 = (int*) malloc(sizeof(int)*1);
    *variable_axuliar_2 = numeros[1];
    int *result_MCD = (int*) malloc(sizeof(int)*1);
    *result_MCD = 1;
    int *result_2 = (int*) malloc(sizeof(int)*1);
    int *auxiliar_3 = (int*) malloc(sizeof(int)*1);
    if(numeros[0] > numeros[1]){
        *variable_axuliar_1 = numeros[0];
        *variable_axuliar_2 = numeros[1];
    } else{
        *variable_axuliar_1 = numeros[1];
        *variable_axuliar_2 = numeros[0];;
    };
    *result_2= *variable_axuliar_1 % *variable_axuliar_2;
    *result_MCD = *variable_axuliar_2;
    while (*result_2 != 0){
        *auxiliar_3 = *result_MCD;
        *result_MCD =  *result_2;
        *result_2 = *variable_axuliar_1 % *result_MCD;
    }
    return result_MCD;
}

int *hallar_mcm(int *numeros){
    int y=1, i=2;
    int z= numeros[0];
    int x= numeros[1];
     do{
            if (x%i==0){
                if(z%i==0){
                    y=y*i;
                    x=x/i; z=z/i;
                    i=2;
                }
                else i++;
            }
            else i++;
        } while(i<=x);
    int *result = (int*)malloc(sizeof(int)*1);
    *result =y*z*x;
    return result;
}

MCD_MCM *hallar_mcd_mcm(int *numeros){
    MCD_MCM *mcd_mcm = (MCD_MCM*)malloc(sizeof(MCD_MCM));
    mcd_mcm->MCM = (int*)malloc(sizeof(int));
    *mcd_mcm->MCM = *hallar_mcm(numeros);
    mcd_mcm->MCD = (int*)malloc(sizeof(int));
    *mcd_mcm->MCD = *hallar_mcd(numeros);
    return mcd_mcm;
}

void* hilo_1(void *unused){
    int *numero_fibonacci;
    numero_fibonacci = (int*)unused;
    int *frecuencia;
    frecuencia = (int*)malloc(sizeof(int));
    int *result;
    result = (int*)malloc(sizeof(int));
    *frecuencia = 1;
    *result = *hace_parte_de_fibonacci(numero_fibonacci);

    resultado_fibonacci = result;
    free(frecuencia);
    return NULL;
}



void* hilo_2(void* unused){
    int *numero_ingresado;
    numero_ingresado = (int*)unused;
    int *frecuencia;
    frecuencia = (int*)malloc(sizeof(int));
    *frecuencia = 1;
    int *result = es_primo_i(numero_ingresado);

    resultado_primo = result;
    free(frecuencia);
    return NULL;
}

void* hilo_3(void* unused){
    int *numeros_mcd_mcm;
    numeros_mcd_mcm = (int*)unused;
    int *frecuencia;
    frecuencia = (int*)malloc(sizeof(int));
    *frecuencia = 1;
    MCD_MCM *result = hallar_mcd_mcm(numeros_mcd_mcm);

    resultado_mcd_mcm = result;
    free(frecuencia);
    return NULL;
}



int main() {
    pthread_t thread_id_1;
    pthread_t thread_id_2;
    pthread_t thread_id_3;
    int *numero_fibonacci = (int*) malloc(sizeof(int)*1);
    int *numero_primo = (int*) malloc(sizeof(int)*1);
    int *numeros_mcd_mcm = (int*) malloc(sizeof(int)*2);
    printf("\nIngre un numero el cual isted quiera saber si hace parte de la serie fibonacci : ");
    scanf("%d", numero_fibonacci);
    printf("\n");
    printf("\nIngrese un numero el cual quiera saber si es primo : ");
    scanf("%d", numero_primo);
    printf("\n");
    printf("\nIngrese el primer numero al cual quiera hallerle su MCM y MCD : ");
    scanf("%d", &numeros_mcd_mcm[0]);
    printf("\nIngrese el el segundo numero al cual quiera hallerle su MCM y MCD : ");
    scanf("%d", &numeros_mcd_mcm[1]);
    printf("\n");
    //scanf("%d", );
    pthread_create(&thread_id_1, NULL, &hilo_1, numero_fibonacci);
    pthread_create(&thread_id_2, NULL, &hilo_2, numero_primo);
    pthread_create(&thread_id_3, NULL, &hilo_3, numeros_mcd_mcm);
    pthread_join (thread_id_1, NULL);
    pthread_join (thread_id_2, NULL);
    pthread_join (thread_id_3, NULL);

     if(*resultado_fibonacci == 1){
        printf("\n EL numero %d si hace parte de la serie fibonacci.", *numero_fibonacci);
    }else {
        printf("\n EL numero %d no hace parte de la serie fibonacci.", *numero_fibonacci);
    }

    printf("\n");

    if(*resultado_primo == 1){
        printf("\n EL numero %d si es primo.", *numero_primo);
    }else {
        printf("\n EL numero %d no es primo.", *numero_primo);
    }

    printf("\n");

    printf("Los numeros %d y %d tienen un MCD igual a %d, y un MCM igual a %d.",
           numeros_mcd_mcm[0], numeros_mcd_mcm[1], *resultado_mcd_mcm->MCD, *resultado_mcd_mcm->MCM);

    free(numero_fibonacci);
    free(numero_primo);
    free(numeros_mcd_mcm);
    printf("\n");
    return  0;
}
