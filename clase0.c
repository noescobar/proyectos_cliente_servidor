#include <stdio.h>
#include <math.h>

/*
 * Taller 0:
 *
 * Recibir un programa que permita recibir dos numeros de teclado y realizar las siguientes:
 *
 * Suma, resta, multiplicacion, bdivision, potenciacion, logaritmcion.
 *
 * nota: todas las variables deben ser APUNTADORES.
 *
 * */



float suma(float *a, float *b){
    return *a+*b;
}

float resta(float *a, float *b){
    return *a-*b;
}

float multiplicacion(float *a, float *b){
    return *a**b;
}

float division(float *a, float *b){
    return *a/ *b;
}


float potenciacion(float *base, float *potencia){
    return powf(*base, *potencia);
}

float logaritmacion(float *base, float *numero){
    return logf(*numero)/logf(*base);
}

int main(){
    int opcion;
    float a, b, result;
    printf("0. Salir.\n");
    printf("1. Sumar dos numeros.\n");
    printf("2. Restar dos numeros.\n");
    printf("3. Multiplicacion entre dos numeros.\n");
    printf("4. Dividir dos numeros.\n");
    printf("5. Sacar potencia de un numero.\n");
    printf("6. Realizar el logritmo de dos numeros.\n");
    printf("\nElige una opcion para ser ejecutadas.:\n");
    scanf("%d",&opcion);
    switch (opcion){
        case 1:
            printf("Ingrese el primer numero:  \n");
            scanf("%f",&a);
            printf("Ingrese el segundo numero: \n");
            scanf("%f",&b);
            result =  suma(&a, &b);
            printf("El resultado es: %f\n", result);
            break;
        case 2:
            printf("Ingrese el primer numero:  \n");
            scanf("%f",&a);
            printf("Ingrese el segundo numero: \n");
            scanf("%f",&b);
            result =  resta(&a, &b);
            printf("El resultado es: %f\n", result);
            break;
        case 3:
            printf("Ingrese el primer numero:  \n");
            scanf("%f",&a);
            printf("Ingrese el segundo numero: \n");
            scanf("%f",&b);
            result =  multiplicacion(&a, &b);
            printf("El resultado es: %f\n", result);
            break;
        case 4:
            printf("Ingrese el primer numero:  \n");
            scanf("%f",&a);
            printf("Ingrese el segundo numero: \n");
            scanf("%f",&b);
            result =  division(&a, &b);
            printf("El resultado es: %f\n", result);
            break;
        case 5:
            printf("Ingrese el numero base:  \n");
            scanf("%f",&a);
            printf("Ingrese el numero potencia: \n");
            scanf("%f",&b);
            result =  potenciacion(&a, &b);
            printf("El resultado es: %f\n", result);
            break;
        case 6:
            printf("Ingrese el numero base:  \n");
            scanf("%f",&a);
            printf("Ingrese el numero: \n");
            scanf("%f",&b);
            result =  logaritmacion(&a, &b);
            printf("El resultado es: %f\n", result);
            break;
        default:
            break;
    }

    return  0;
}
