#include <stdio.h>
// #include <math.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>


/*
 * Taller 3:
 *
 * Utilizando solo punteros, Construir el juego del ahorcado.
 *
 * */


void *jugar(char *palabra, int *numero_de_intentos){
    char  *letras_acertadas;
    letras_acertadas = (char*) malloc(sizeof(char)*14);
    char *letra_auxiliar;
    letra_auxiliar = (char*) malloc(sizeof(char)*1);
    int *numero_letras_escondidas;
    numero_letras_escondidas = (int*) malloc(sizeof(int));
    printf("funciona");
    while(*numero_de_intentos > 0){
        int *i;
        i = (int*) malloc(sizeof(int));
        system("clear");
        printf("\n");
        *numero_letras_escondidas = 0;
        for(*i=0; *i < strlen(palabra); *i = *i +1){

            *letra_auxiliar= palabra[*i];
            if(strstr(letras_acertadas,letra_auxiliar) != NULL){
                printf(" %s ", letra_auxiliar);
            }else{
                printf(" _ ");
                *numero_letras_escondidas = *numero_letras_escondidas + 1;
            }

        };
        if(*numero_letras_escondidas > 0) {
            printf("\n Usted tiene %d numero de intentos, ingrese una letra para intentar: ", *numero_de_intentos);
            scanf("%c", letra_auxiliar);
            scanf("%c", letra_auxiliar);
            if (strstr(letras_acertadas, letra_auxiliar) != NULL) {
                printf("Ya ha utilizado esa letra anteriormente");
            } else {
                if (strstr(palabra, letra_auxiliar) != NULL) {
                    printf("Es correcto, oprima enter para continuar");
                    letras_acertadas[strlen(letras_acertadas)] = *letra_auxiliar;
                } else {
                    *numero_de_intentos = *numero_de_intentos - 1;
                }
            }
        }
        else {
            printf("\n\nFelicitaciones a terminado el juego con %d numero de intentos restantes\n\n", *numero_de_intentos);
            break;
        }
    }
    if(*numero_de_intentos <= 0){
        printf("\n\n Has sido ahoracado\n\nx");
    }
    return NULL;
}


int main() {
    int *numero_de_intentos;
    numero_de_intentos = (int *) malloc(sizeof(int));
    *numero_de_intentos = 0;
    char *palabra;
    palabra = (char *) malloc(sizeof(char) * 25);

    printf(" _________________________________________ \n");
    printf("|          Bienvenido al ahorcado         |\n");
    printf("|_________________________________________|\n");
    printf("\n por favor ingrese la palabra con la que va a jugar : ");
    scanf("%s", palabra);
    printf("\n por favor ingrese el numero de intentos :");
    char *input;
    input = (char*) malloc(sizeof(char)*4);
    int *length;
    int *i;
    length = (int*)malloc(sizeof(int));
    i = (int*)malloc(sizeof(int));
    int *error;
    error = (int*)malloc(sizeof(int));
    *error = 1;
    while(*error == 1) {
        scanf ("%s", input);
        *length = strlen (input);
        for (*i = 0; *i < *length; *i = *i + 1) {
            if (!isdigit(input[*i])) {
                printf("\nHa ingresado letras enlugar de un numero, ingrese un valor valido : ");
                *error = 1;
                break;
            }
            else{
                *error = 0;
            }
        }
    }
    *numero_de_intentos = atoi(input);
    system("clear");

    jugar(palabra, numero_de_intentos);

    return  0;
}
