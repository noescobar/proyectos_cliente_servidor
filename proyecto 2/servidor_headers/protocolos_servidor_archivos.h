#include "utilidades_servidor.h"


Cliente *protocolo_crear_cliente(int *socket, struct sockaddr_in from){
    char *direccion_cliente = (char *) malloc(sizeof(char)*30);
    Cliente *cliente_creado= (Cliente *) malloc(sizeof(Cliente)*1);
    cliente_creado->socket_id = socket;
    sprintf(direccion_cliente,"%s:%d",inet_ntoa(from.sin_addr), from.sin_port);
    cliente_creado->ip_address = direccion_cliente;
    cliente_creado->transfiriendo = (int *) malloc(sizeof(int)*1);
    ListaClienteObject *objeto_auxiliar = NULL;
    objeto_auxiliar = clientes_contectados->cabeza;
    while(objeto_auxiliar != NULL){
        if(objeto_auxiliar->cliente->socket_id == socket){
            free(cliente_creado);
            cliente_creado = objeto_auxiliar->cliente;
            break;
        }
        objeto_auxiliar = objeto_auxiliar->siguiente;
    }
    registrar_cliente(conector_global,cliente_creado);;
    agregar_cliente(clientes_contectados,cliente_creado);


    return  cliente_creado;
}


void protocolo_registrar_archivos(Cliente *cliente_owner){
    char *archivos = hacer_peticion_cadena(cliente_owner->socket_id,"$ ARCHIVOS", 0);
    registrar_archivos_bd(conector_global,cliente_owner,split(archivos,' '));
}


void protocolo_eliminar_cliente(int *socket, struct sockaddr_in from){

}


char *protocolo_obtener_archivo(Cliente *cliente, char **parametros){
    char *archivo_bd=NULL;
    if(get_len(parametros) >2){
        if(strcmp(parametros[1],"-i")==0){
            int *id = (int *) malloc(sizeof(int)*1);
            *id = atoi(parametros[2]);
            archivo_bd = get_archivo(conector_global,id,NULL);
        }else{
            mandar_mensaje(cliente->socket_id,"Ha ingresado un parametro extra desconocido");
        }
    }else{
        archivo_bd = get_archivo(conector_global,NULL, parametros[1]);
    }
    return archivo_bd;
}


void protocolo_abrir_archivo(Cliente *cliente, char **parametros){
    char *archivo_bd;
    char *direccion_cliente = cliente->ip_address;
    char *cadena_recibida;
    char *mensaje_envio = (char *) malloc(sizeof(char)*1024);
    char *cadena_aux = (char *) malloc(sizeof(char)*1024);
    Cliente *receptor = cliente;
    Cliente *emisor = NULL;
    archivo_bd = protocolo_obtener_archivo(cliente,parametros);

    if(archivo_bd == NULL){
        mandar_mensaje(cliente->socket_id, "No se ha podido encontrar el archivo.");
        print_server("GET",direccion_cliente,"ABRIR","ERROR", "NO SE HA ENCONTRADO EL ARCHIVO");
    }else if(archivo_bd[0] == '#'){
        archivo_bd[0] = ' ';
        print_server("GET",direccion_cliente,"ABRIR",NULL, "MULTIPLES ARCHIVOS ENCONTRADOS");
        sprintf(mensaje_envio, "%s\nIngrese el id del archivo.", archivo_bd);
        cadena_recibida = hacer_peticion_cadena(cliente->socket_id, mensaje_envio, 0);
        sprintf(cadena_aux,"listar -i %s", cadena_recibida);
        archivo_bd = protocolo_obtener_archivo(cliente,split(cadena_aux,' '));
        if(archivo_bd == NULL) {
            print_server("GET", direccion_cliente, "ABRIR", "ERROR", "ID ARCHIVO INVALIDO");
            mandar_mensaje(cliente->socket_id, "El id ingresado es invalido");
        }
    }

    if(archivo_bd != NULL){
        char **arhivo_split = split(archivo_bd,' ');
        ListaClienteObject *objeto_auxiliar = clientes_contectados->cabeza;
        while (objeto_auxiliar != NULL) {
            if (objeto_auxiliar->cliente != NULL) {
                printf("\nlista --> %d archivo -->%s\n",*objeto_auxiliar->cliente->session_key ,arhivo_split[0]);
                printf("\nlista --> %d archivo -->%d\n",*objeto_auxiliar->cliente->session_key ,atoi(arhivo_split[0]));
                if (atoi(objeto_auxiliar->cliente->session_key) == atoi(arhivo_split[2])) {
                    emisor = objeto_auxiliar->cliente;
                    break;
                }
            }
            objeto_auxiliar = objeto_auxiliar->siguiente;
        }
        if(emisor == receptor){

            clean_string(mensaje_envio);
            sprintf(mensaje_envio,"$ IMPRIMIR_ARCHIVO %s", arhivo_split[1]);
            mandar_mensaje(receptor->socket_id, mensaje_envio);
        }else {
            transferir_archivo(emisor, receptor, arhivo_split[1]);
        }
    }
}


void protocolo_eliminar_archivo(Cliente *cliente, char **parametros){
    char *archivo_bd;
    char *direccion_cliente = cliente->ip_address;
    char *cadena_recibida;
    char *mensaje_envio = (char *) malloc(sizeof(char)*1024);
    char *cadena_aux = (char *) malloc(sizeof(char)*1024);
    Cliente *solicitante = cliente;
    Cliente *propietario = NULL;
    archivo_bd = protocolo_obtener_archivo(cliente,parametros);
    if(archivo_bd == NULL){
        mandar_mensaje(cliente->socket_id, "No se ha podido encontrar el archivo.");
        print_server("DELETE",direccion_cliente,"ELIMIAR","ERROR", "NO SE HA ENCONTRADO EL ARCHIVO");
    }else if(archivo_bd[0] == '#'){
        archivo_bd[0] = ' ';
        print_server("DELETE",direccion_cliente,"ELIMINAR",NULL, "MULTIPLES ARCHIVOS ENCONTRADOS");
        sprintf(mensaje_envio, "%s\nIngrese el id del archivo.", archivo_bd);
        cadena_recibida = hacer_peticion_cadena(cliente->socket_id, mensaje_envio, 0);
        sprintf(cadena_aux,"listar -i %s", cadena_recibida);
        archivo_bd = protocolo_obtener_archivo(cliente,split(cadena_aux,' '));
        if(archivo_bd == NULL){
            print_server("DELETE",direccion_cliente,"ELIMINAR","ERROR", "ID ARCHIVO INVALIDO");
            mandar_mensaje(cliente->socket_id,"El id ingresado es invalido");
        }
    }

    if(archivo_bd != NULL){
        char **arhivo_split = split(archivo_bd,' ');
        ListaClienteObject *objeto_auxiliar = clientes_contectados->cabeza;
        while (objeto_auxiliar != NULL) {
            if (objeto_auxiliar->cliente != NULL) {
                if (atoi(objeto_auxiliar->cliente->session_key) == atoi(arhivo_split[2])) {
                    propietario = objeto_auxiliar->cliente;
                    break;
                }
            }
            objeto_auxiliar = objeto_auxiliar->siguiente;
        }
        if(propietario != NULL) {
            clean_string(mensaje_envio);
            sprintf(mensaje_envio, "$ ELIMINAR %s", arhivo_split[1]);
            cadena_recibida =hacer_peticion_cadena(propietario->socket_id, mensaje_envio, 0);
            if(strcmp(cadena_recibida,"SUCCESS")){
                print_server("DELETE",direccion_cliente,"ELIMINAR","SUCCESS", "ARCHIVO ELIMINADO");
                mandar_mensaje(solicitante->socket_id,"SE A ELIMINADO EL ARCHIVO CON EXITO");
            }else{
                print_server("DELETE",direccion_cliente,"ELIMINAR","SUCCESS", "ARCHIVO ELIMINADO");
                mandar_mensaje(solicitante->socket_id,"HA OCURRIDO UN ERROR ELIMINANDO EL ARCHIVO");
            }
        }else{
            print_server("DELETE",direccion_cliente,"ELIMINAR","ERROR", "PROPIETARIO DEL ARCHIVO DESCONECTADO");
            mandar_mensaje(solicitante->socket_id,"El cliente prpietario del archivo, no se encuentra conectado");
        }
    }
}


void protocolo_listar_archivos(Cliente *cliente_actual){
    char *lista = listar_archivos_bd(conector_global);
    mandar_mensaje(cliente_actual->socket_id, lista);
}



void protocolos_server(int *socket, struct sockaddr_in from){

    char **parametros;
    char *direccion_cliente = (char *) malloc(sizeof(char)*30);
    char *cadena_envio= (char *)malloc(sizeof(char)*1024);
    Cliente *cliente_session= protocolo_crear_cliente(socket,from);
    print_server("GET",direccion_cliente,"REGISTRAR","SUCCESS","SE HA REGISTRADO EL USUARIO CON EXITO");
    sprintf(direccion_cliente,"%s:%d",inet_ntoa(from.sin_addr), from.sin_port);

    protocolo_registrar_archivos(cliente_session);

    print_server("POST",direccion_cliente,"ALMACENAR","SUCCESS", "ALMACENADOS LOS ARCHIVOS.");

    while(1) {
        sprintf(cadena_envio,"");

        parametros = leer_comandos(socket, &from);
        if(parametros != NULL) {
            // protocolos
            if(strcmp(parametros[0],"listar") == 0){
                protocolo_listar_archivos(cliente_session);
                print_server("GET",direccion_cliente,"LISTAR","SUCCESS", "SE HAN LISTADO CON EXITO");
            } else if(strcmp(parametros[0],"abrir") == 0){
                protocolo_abrir_archivo(cliente_session, parametros);
            } else if(strcmp(parametros[0],"eliminar") == 0){
                protocolo_eliminar_archivo(cliente_session,parametros);
                print_server("DELETE",direccion_cliente,"ELIMINAR",NULL, "SE HA ELIMINADO EL ARCHIVO CON EXITO");
            } else if(strcmp(parametros[0],"exit") == 0){
                mandar_mensaje(socket,"Se ha terminado");
                break;
            } else{
                mandar_mensaje(socket,"comando invalido");
                print_server("GET/POST/DELETE",direccion_cliente,parametros[0],NULL, "SE HA INGRESADO UN COMANDO INVALIDO");
            }

        }else{

            break;
        }
    }
    remover_usuario(clientes_contectados,cliente_session);
    close(*socket);
    free(direccion_cliente);
    free(cadena_envio);
    free(cliente_session);
}