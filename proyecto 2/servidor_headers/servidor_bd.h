#include "core_servidor.h"


/*
 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
 *  funcion establecer_conexion                                                                                        *
 *                                                                                                                     *
 *      descripcion: Esta funcion permite establecer una conexion directa con la base de datos haciendo uso de las     *
 *                   variables globales definidas al principio del codigo que describen la conexion con la base de     *
 *                   datos postgres.                                                                                   *
 *      retorno    : Se retorna un puntero que apunta directamente al conector de la base de datos y que permitira     *
 *                   manipularla desde el codigo c.                                                                    *
 *                                                                                                                     *
 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
*/

PGconn *establecer_conexion(){
    PGconn *conn;

    conn = PQsetdbLogin(HOST_PG,PORT_PG,NULL,NULL,DATA_BASE_PG, USER_PG, PASSWORD_PG);

    if (PQstatus(conn) != CONNECTION_BAD)
    {
        return conn;
    }else{
        printf("\n No se a podido establecer conexion con la base de datos.\n");
        exit(-1);
    }
}




Cliente *registrar_cliente(PGconn *conector, Cliente *cliente_actual){
    PGresult *res;
    int resultado=0;
    char *consulta= (char *)malloc(sizeof(char)*1024);
    Cliente *cliente;

    if(cliente_actual != NULL){
        cliente = cliente_actual;
    }else{
        cliente = (Cliente *) malloc(sizeof(Cliente));
    }

    if (PQstatus(conector) != CONNECTION_BAD) {
        sprintf(consulta, "with rows as (INSERT INTO session_client (ip_address,active) VALUES('%s',TRUE) RETURNING id) SELECT "
                "id FROM rows",cliente->ip_address);
        res = PQexec(conector, consulta);
        cliente->session_key = PQgetvalue(res,0,0);
        printf("\n %s \n", cliente->session_key);
    }

    free(consulta);

    return cliente;

}



void registrar_archivos_bd(PGconn *conector, Cliente *cliente_actual,char **archivos){
    PGresult *res;
    int resultado=0;
    char *consulta= (char *)malloc(sizeof(char)*1024);
    Cliente *cliente;
    int i=0;
    if(cliente_actual != NULL){
        cliente = cliente_actual;
    }else{
        cliente = (Cliente *) malloc(sizeof(Cliente));
    }
    sprintf(consulta,"INSERT INTO archivos (session_owner_id, nombre) VALUES");
    if (PQstatus(conector) != CONNECTION_BAD) {
        while(archivos[i] != NULL) {
            if (archivos[i+1] != NULL) {
                sprintf(consulta, "%s(%s,'%s'),",consulta, cliente->session_key, archivos[i]);
            } else {
                sprintf(consulta, "%s(%s,'%s');",consulta, cliente->session_key, archivos[i]);
            }
            i++;
        };
        res = PQexec(conector, consulta);
    }

    free(consulta);
}



char *listar_archivos_bd(PGconn *conector){
    PGresult *res;
    int max_cadena = 1024;
    int paquetes = 1024;
    char *cadena_auxiliar;
    char *resultado= (char *)malloc(sizeof(char)*1024);
    char *consulta= (char *)malloc(sizeof(char)*1024);
    Cliente *cliente;
    int *i = (int *)malloc(sizeof(int)*1);
    *i = 0;
    res = PQexec(conector, "select id, nombre from archivos");
    sprintf(resultado,"id\tarchivo");
    if (res != NULL && PGRES_TUPLES_OK == PQresultStatus(res)) {
        for (*i = 0; *i < PQntuples(res); (*i)++) {
            sprintf(resultado, "%s\n%s\t|%s",resultado, PQgetvalue(res, *i,0), PQgetvalue(res, *i,1));
            if(strlen(resultado) >= (max_cadena*paquetes - 100)){
                cadena_auxiliar = resultado;
                paquetes++;
                resultado = (char *) malloc(sizeof(char)*(max_cadena*paquetes));
                sprintf(resultado,"%s", cadena_auxiliar);
                free(cadena_auxiliar);
                cadena_auxiliar = NULL;
            }
        }
    }


    free(consulta);
    return  resultado;
}



char *get_archivo(PGconn *conector, int *id, char *nombre) {
    PGresult *res;
    int max_cadena = 1024;
    int paquetes = 1;
    char *resultado = (char *) malloc(sizeof(char) * 1024);
    char *consulta = (char *) malloc(sizeof(char) * 1024);;
    char *cadena_auxiliar;
    int *i = (int *) malloc(sizeof(int) * 1);
    *i = 0;
    if (id != NULL) {
        sprintf(consulta, "select id, nombre, session_owner_id from archivos where id=%d", *id);
        res = PQexec(conector, consulta);
    } else if (nombre != NULL) {
        sprintf(consulta, "select id, nombre, session_owner_id  from archivos where  lower(nombre) = lower('%s')", nombre);
        res = PQexec(conector, consulta);
    } else {
        res = NULL;
    }

    if (res != NULL && PGRES_TUPLES_OK == PQresultStatus(res)) {
        if (PQntuples(res) > 1) {
            sprintf(resultado, "#"); /// esto quiere decir que el resultado fue mayor a 1
            for (*i = 0; *i < PQntuples(res); (*i)++) {
                sprintf(resultado, "%s\n%s\t|%s",resultado, PQgetvalue(res, *i,0), PQgetvalue(res, *i,1));
                if(strlen(resultado) >= (max_cadena*paquetes - 100)){
                    cadena_auxiliar = resultado;
                    paquetes++;
                    resultado = (char *) malloc(sizeof(char)*(max_cadena*paquetes));
                    sprintf(resultado,"%s", cadena_auxiliar);
                    free(cadena_auxiliar);
                    cadena_auxiliar = NULL;
                }
            }

        } else if (PQntuples(res) == 1) {
            sprintf(resultado, "%s %s %s", PQgetvalue(res, 0, 0), PQgetvalue(res, 0, 1), PQgetvalue(res, 0, 2));
        }else{
            free(resultado);
            resultado = NULL;
        }
    } else {
        free(resultado);
        resultado = NULL;
    }


    free(consulta);
    return resultado;
}



void limpiar_tablas(PGconn *conector){
    PGresult *res;
    char *consulta= (char *)malloc(sizeof(char)*1024);




    if (PQstatus(conector) != CONNECTION_BAD) {
        sprintf(consulta, "TRUNCATE session_client CASCADE");
        res = PQexec(conector, consulta);
    }

    free(consulta);
}