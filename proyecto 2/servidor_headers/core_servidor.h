#include <sys/time.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <string.h>
#include <pthread.h>
#include <libpq-fe.h>
#include <ctype.h>


#define MAXHOSTNAME 80

#define DEFAULT_HOST "127.0.0.1";
#define DEFAULT_PORT "5000";

#define USER_PG "postgres"
#define PASSWORD_PG "pswd123"
#define DATA_BASE_PG "archivos"
#define PORT_PG "5432"
#define HOST_PG "127.0.0.1"


#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

struct Cliente{
    char *session_key;
    char *ip_address;
    char *port;
    int *socket_id;
    int *transfiriendo;
};
typedef struct Cliente Cliente;


struct ListaClientes {
    struct ListaClienteObject *cabeza;
    struct ListaClienteObject *cola;
    int tamano;
};
typedef struct ListaClientes ListaClientes;

struct ListaClienteObject{
    ListaClientes *cabecera;
    struct ListaClienteObject *anterior;
    struct ListaClienteObject *siguiente;
    Cliente *cliente;
};
typedef struct ListaClienteObject ListaClienteObject;


PGconn *conector_global;
ListaClientes *clientes_contectados;

void agregar_cliente(struct ListaClientes *lista,Cliente *objeto){
    ListaClienteObject *objecto_lista = (ListaClienteObject *) malloc(sizeof(ListaClienteObject));
    objecto_lista->cliente = objeto;

    if(lista->cabeza == NULL){
        if(lista->cola != NULL){
            free(lista->cola);
            lista->cola = NULL;
        }
        lista->cabeza = objecto_lista;
        lista->cola = objecto_lista;
        objecto_lista->cabecera = lista;
        objecto_lista->cabecera = lista;
        objecto_lista->anterior = NULL;
        objecto_lista->siguiente = NULL;
        lista->tamano = 1;
    }else{
        if(lista->cola != NULL){
            lista->cola->siguiente = objecto_lista;
            objecto_lista->anterior = lista->cola;
            lista->cola = objecto_lista;
            objecto_lista->siguiente = NULL;
            lista->tamano++;
        }
    }
}


void remover_usuario(ListaClientes *lista,Cliente *objeto){
    ListaClienteObject *objecto_lista =lista->cabeza;
    while(objecto_lista != NULL){
        if(objeto == objecto_lista->cliente){
            if(objecto_lista == lista->cabeza){
                lista->cabeza = objecto_lista->siguiente;
            }
            if(objecto_lista == lista->cola){
                lista->cola = objecto_lista->anterior;
            }
            if(objecto_lista->anterior != NULL){
                objecto_lista->anterior->siguiente = objecto_lista->siguiente;
            }
            if(objecto_lista->siguiente != NULL){
                objecto_lista->siguiente->anterior = objecto_lista->anterior;
            }
            lista->tamano --;
            break;
        }
        objecto_lista = objecto_lista->siguiente;
    }
}

void clean_string(char *cadena){
    if(cadena != NULL){
        int largo = (int)strlen(cadena);
        for(int i=0; i< largo;i++){
            cadena[i]='\0';
        }
    }

}

/*
 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
 *  funcion mostrar_cliente_list:                                                                                      *
 *                                                                                                                     *
 *      descripcion: Como se puede ver en el nombre esta funcion me permite mostrar los usuario contenidos en una n de *
 *                   estructura de  tipo ListaClientes, esta funcion me permite de forma simple comprobar los usuarios *
 *                   dentro de una lista.                                                                              *                                           *
 *                                                                                                                     *
 *      Parametros:                                                                                                    *
 *          lista: Esta  es  la  lista  que  contiene  a los usuario siendo esta la que recorreremos para mostrar cada *
 *                 contenido en esta misma.                                                                            *
 *                                                                                                                     *
 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
*/

void mostrar_cliente_list(ListaClientes *lista){
    ListaClienteObject *lista_auxiliar = lista->cabeza;
    while(lista_auxiliar!= NULL){
        printf("\n");
        printf("%i \t",*lista_auxiliar->cliente->session_key);
        printf("\n");
        lista_auxiliar = lista_auxiliar->siguiente;
    }
}


/*  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
 *                                                                                                                     *
 *  funcion **split:                                                                                                   *
 *                                                                                                                     *
 *      descripcion: Esta funcion toma un arreglo de caracteres que podemos llamar string y lo parte en un vector de   *
 *                   strings partiendolo apartir de un  caracter que nosotros mismo definimos(para mas ejemplos se     *
 *                   puede tomar como referencia la funcion split de python).                                          *
 *                                                                                                                     *
 *      parametros :                                                                                                   *
 *          *string : Este es el arreglo de caracteres que queromos partir apartir de un simbolo                       *
 *          sep     : Este es el simbolo con el cual tomaremos como referencia para partir la cadena.                  *
 *                                                                                                                     *
 *      retorno : El resultado de esta funcion es un vector de cadena de caracteres que en cada posicion tendra los    *
 *                strings resultantes de separar la cadena principal.                                                  *
 *                                                                                                                     *
 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
*/

char **split ( char *string, const char sep) {

    char **lista;
    char *p = string;
    int i = 0;

    int pos;
    const int len = (int)strlen (string);

    lista = (char **) malloc (sizeof (char *));
    if (lista == NULL) { /* Cannot allocate memory */
        return NULL;
    }

    lista[pos=0] = NULL;

    while (i < len) {

        while ((p[i] == sep) && (i < len))
            i++;

        if (i < len) {

            char **tmp = (char **) realloc (lista , (pos + 2) * sizeof (char *));
            if (tmp == NULL) { /* Cannot allocate memory */
                free (lista);
                return NULL;
            }
            lista = tmp;
            //tmp = NULL;

            lista[pos + 1] = NULL;
            lista[pos] = (char *) malloc (sizeof (char));
            if (lista[pos] == NULL) { /* Cannot allocate memory */
                for (i = 0; i < pos; i++)
                    free (lista[i]);
                free (lista);
                return NULL;
            }

            int j = 0;
            for (i; ((p[i] != sep) && (i < len)); i++) {
                lista[pos][j] = p[i];
                j++;

                char *tmp2 = (char *) realloc (lista[pos],(j + 1) * sizeof (char));
                if (lista[pos] == NULL) { /* Cannot allocate memory */
                    for (i = 0; i < pos; i++)
                        free (lista[i]);
                    free (lista);
                    return NULL;
                }
                lista[pos] = tmp2;
                //tmp2 = NULL;
            }
            lista[pos][j] = '\0';
            pos++;
        }
    }

    return lista;
}

/*  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
 *                                                                                                                     *
 *  funcion get_len :                                                                                                  *
 *                                                                                                                     *
 *      descripcion: Esta funcion me permite obtener el tamano de un arreglo(especificamente de strings) se creo con el*
 *                   el objtivo de saber el tamano de uno para que se pudiera recorrer, de forma adecuada.             *
 *                                                                                                                     *
 *      parametros :                                                                                                   *
 *          **x : Este es el arreglo de strings al cual queremos calcularle el tamano.                                 *
 *                                                                                                                     *
 *      retorno : Entero i siendo i el resultado con el tamano del  arreglo                                            *
 *                                                                                                                     *
 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
*/

int get_len(char **x){ int i=0; while(x[i] != NULL){ i++;}return i;}



void mandar_mensaje(int *socket, const char *mensaje){
    char *buf = (char *)malloc(sizeof(char)*1024);
    sprintf(buf,"%s",mensaje);


    if (send(*socket, buf,1024, 0) <0 ){
        perror("sending stream message");
    }
}



void reusePort(int s)
{
    int one=1;

    if ( setsockopt(s,SOL_SOCKET,SO_REUSEADDR,(char *) &one,sizeof(one)) ==
         -1 )
    {
        printf("error in setsockopt,SO_REUSEPORT \n");
        exit(-1);
    }
}


int contruir_servidor(struct sockaddr_in from, int fromlen, char *argv[], int argc){
    int sd, psd;
    struct sockaddr_in server;
    struct hostent *hp, *gethostbyname();
    struct servent *sp;
    int length;
    char ThisHost[80];
    const char *port;

     if(argc <2){
        printf("\nInstrucciones de uso: %s  <puerto servidor>", argv[0]);
        printf("\nSe utilizara el puerto por defecto.");
        port = DEFAULT_PORT;
    } else{
        port = argv[1];
    }


    sp = getservbyname("echo", "tcp");
/* get EchoServer Host information, NAME and INET ADDRESS */

    gethostname(ThisHost, MAXHOSTNAME);

/* OR strcpy(ThisHost,"localhost"); */

    printf("----TCP/Server running at host NAME: %s\n", ThisHost);
    if ( (hp = gethostbyname(ThisHost)) == NULL ) {
        fprintf(stderr, "Can't find host %s\n", port);
        exit(-1);
    }
    bcopy ( hp->h_addr, &(server.sin_addr),(socklen_t) hp->h_length);
    printf(" (TCP/Server INET ADDRESS is: %s )\n",
           inet_ntoa(server.sin_addr));

    /* Construct name of socket to send to. */
    server.sin_family = (sa_family_t)hp->h_addrtype;
/*OR server.sin_family = AF_INET; */

    server.sin_addr.s_addr = htonl(INADDR_ANY);

    /* server.sin_port = htons((u_short) 0); i*/
/*OR server.sin_port = sp->s_port; */
    server.sin_port = htons( (u_short) atoi(port));

/* Create socket on which to send and receive */

    /* sd = socket (PF_INET,SOCK_DGRAM,0); */
    sd = socket (hp->h_addrtype,SOCK_STREAM,0);

    if (sd<0) {
        perror("opening stream socket");
        exit(-1);
    }

    reusePort(sd);

    if ( bind( sd,(struct sockaddr *) &server, sizeof(server) ) ) {
        close(sd);
        perror("binding name to stream socket");
        exit(-1);
    }

    length = sizeof(server);
    if ( getsockname (sd,(struct sockaddr *)&server, (socklen_t *) &length) ) {
        perror("getting socket name");
        exit(0);
    }

    printf("\n El puerto del servidor es: %d\n", ntohs(server.sin_port));
    listen(sd,2);
    //fromlen = sizeof(from);
    return  sd;
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                                                     *
 * funcion **leer_comandos :                                                                                           *
 *                                                                                                                     *
 *      Descripcion : Esta funcion permite recibir un cadena de caracteres (ya sea atraves del socket) y convertirla en*
 *                    un vector de strings para su tilizacion en el servidor                                           *
 *                                                                                                                     *
 *      Parametros :                                                                                                   *
 *                                                                                                                     *
 *          *socket : Entero que me permite hacer la utilizacion del socket correspondiente, es utilizado para leer    *
 *                    y mirar lo que se halla mandado al servidor                                                      *
 *                                                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*/

char **leer_comandos(int *socket, struct sockaddr_in *from){
    char *cadena_comando=(char *) malloc(sizeof(char)*1024);
    char **parametros;
    int *i = (int*)malloc(sizeof(int)*1);
    *i = 0;
    int rc;
    if( (rc=(int)read(*socket,cadena_comando, 1024)) < 0) {
        return NULL;
    }
    if (rc > 0 ){
        if(cadena_comando[0] != '\0') {
            cadena_comando[rc] = ' ';
        }
    }
    else {
        close (*socket);
        free(cadena_comando);
        cadena_comando = NULL;
    }
    if(cadena_comando != NULL) {
        while(cadena_comando[*i]){
            if(cadena_comando[*i]=='\n'){
                cadena_comando[*i]= '\0';
            }
            cadena_comando[*i] = (char) tolower(cadena_comando[*i]);
            (*i)++;
        }
        parametros = split(cadena_comando, ' ');

        free(cadena_comando);
        return parametros;
    }else{
        return NULL;
    }
}



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                                                     *
 *                                                                                                                     *
 *                                                                                                                     *
 *                                                                                                                     *
 *                                                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*/

char *leer_cliente(int *socket, int lower){
     char *cadena_de_llegada=(char *) malloc(sizeof(char)*1024);
    int *i = (int*)malloc(sizeof(int)*1);
    *i = 0;
    int rc;
    if( (rc=(int)read(*socket,cadena_de_llegada, 1024)) < 0) {
        return NULL;
    }
    if (rc > 0 ){
        if(cadena_de_llegada[0] != '\0') {
            cadena_de_llegada[rc] = '\0';
        }
    }
    else {
        cadena_de_llegada = NULL;
    }
    if(cadena_de_llegada != NULL) {
        while (cadena_de_llegada[*i]) {
            if (cadena_de_llegada[*i] == '\n') {
                cadena_de_llegada[*i] = '\0';
            }
            if(lower) {
                cadena_de_llegada[*i] = (char) tolower(cadena_de_llegada[*i]);
            }
            (*i)++;
        }
    }
    return  cadena_de_llegada;
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                                                     *
 *                                                                                                                     *
 *                                                                                                                     *
 *                                                                                                                     *
 *                                                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*/

char **hacer_peticion_comandos(int *socket, const char *mensaje){

    mandar_mensaje(socket,mensaje);

    return  leer_comandos(socket,NULL);
}


char *hacer_peticion_cadena(int *socket, const char *mensaje, int lower){
    mandar_mensaje(socket,mensaje);
    return leer_cliente(socket,lower);
}



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                                                     *
 *                                                                                                                     *
 *                                                                                                                     *
 *                                                                                                                     *
 *                                                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*/

void broad_cast_mensaje(ListaClientes *lista_usuarios, const char *mensaje, Cliente *cliente){
    ListaClienteObject *lista_object_auxiliar = lista_usuarios->cabeza;
    while(lista_object_auxiliar != NULL){

        if(lista_object_auxiliar->cliente != cliente) {
            mandar_mensaje(lista_object_auxiliar->cliente->socket_id, mensaje);
        }
        lista_object_auxiliar = lista_object_auxiliar->siguiente;
    };
}



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                                                     *
 *                                                                                                                     *
 *                                                                                                                     *
 *                                                                                                                     *
 *                                                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*/
void transferir_archivo(Cliente *emisor, Cliente *receptor, char *nombre_archivo){
    char *peticion = (char *) malloc(sizeof(char)*1024);
    mandar_mensaje(emisor->socket_id,"Se iniciara, transferencia de archivo, lectura de teclado desactivada.");
    mandar_mensaje(receptor->socket_id,"Se iniciara, transferencia de archivo, lectura de teclado desactivada.");
    mandar_mensaje(receptor->socket_id,"$ ABRIR");
    sprintf(peticion,"$ TRANSFERIR %s",nombre_archivo);
    char *archivo=hacer_peticion_cadena(emisor->socket_id,peticion,0);
    mandar_mensaje(receptor->socket_id,archivo);
    free(archivo);
    free(peticion);

}


void print_RED(const char *cadena){

    printf(ANSI_COLOR_RED "%s" ANSI_COLOR_RESET ,cadena);
}


void print_GREEN(const char *cadena){
    printf(ANSI_COLOR_GREEN "%s" ANSI_COLOR_RESET ,cadena);
}

void print_YELLOW(const char *cadena){

    printf(ANSI_COLOR_YELLOW "%s" ANSI_COLOR_RESET ,cadena);
}


void print_server(const char *metodo, const char *ip_client,const char *peticion,const char *estado, char *extras){
    if(estado != NULL) {
        if (strcmp(estado, "ERROR") == 0) {
            printf("\n[");
            print_RED(metodo);
            printf("]");
            printf(" ");
            printf(ip_client);
            printf(" : ");
            printf(peticion);
        } else if (strcmp(estado, "SUCCESS")==0) {;
            printf("\n[");
            print_GREEN(metodo);
            printf("]");
            printf(" ");
            printf(ip_client);
            printf(" : ");
            printf(peticion);
        }
    }else{
        printf("\n[");
        print_YELLOW(metodo);
        printf("]");
        printf(" ");
        printf(ip_client);
        printf(" : ");
        printf(peticion);
    }
    if(extras != NULL) {
        printf(" --> %s", extras);
    }
    puts("\n");
}

