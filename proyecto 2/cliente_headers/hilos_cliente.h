#include "core_cliente.h"



char *leer_teclado(char *key, char* tipo, const char *simbolo){
    char *cadena_retorno = (char *)malloc(sizeof(char)*1024);
    if(tipo != NULL){
        if(strcmp(tipo,"h")==0){
            cadena_retorno = scantf_hidden(key,simbolo[0]);
        }
    } else{
        printf("\n%s",key);
        fgets(cadena_retorno,1024,stdin);

    }

    return  cadena_retorno;

};


void *hilo_lector_teclado(void *socket){
    mensaje_recibido = 1;
    int *socket_pointer = (int *) socket;
    int socket_id = *socket_pointer;
    char *buf_auxiliar = (char*) malloc(sizeof(char)*BUFSIZE);
    char **parametros_from_server;
    char **parametros_cliente;
    int i=0;
    while(1){
        if(mensaje_recibido ==1) {
            if( rbuf[0] != '$'){
                cleanup(buf);
                sprintf(buf, "%s", leer_teclado(simbolo_entrada_terminal(username, inet_ntoa(from_global.sin_addr)), NULL, NULL));

                if (buf[0] == '\0') {
                    break;
                }
                parametros_cliente = split(buf, ' ');

                if(strstr(parametros_cliente[0],"clear") != NULL){
                    system("clear");
                    buf[0] = '\n';
                }
            }

            if(buf[0] != '\n' & rbuf[0] != '$') {
                if(transfiriendo == 0) {
                    if (send(socket_id, buf, (size_t) 1024, 0) > 0) {
                        mensaje_recibido = 0;
                    }
                }
            }
        }
    }
    close(socket_id);
    return NULL;
}


const int escuchar_servidor(int *socket){
    int *numero_paquetes = (int *) malloc(sizeof(int)*1);
    char *cadena_envio;
    int cc;
    char **parametros;
    memset(rbuf,'\0',strlen(rbuf));
    cc= (int) recv(*socket, rbuf, sizeof(rbuf), 0);
    if (cc > 0) {
        mensaje_recibido = 1;
        if(transfiriendo == 0){
            if(rbuf[0] == '\0'){
                printf("\nSe a perdido la conexion con el servidor\n");
                return 0;
            }else if(rbuf[0] == '$'){
                parametros = split(rbuf, ' ');
                if(strcmp(parametros[1],"ARCHIVOS") == 0){
                    cadena_envio = lista_archivos_local();
                    mandar_mensaje(socket,cadena_envio);
                }else if(strcmp(parametros[1],"TRANSFERIR") == 0){
                    transfiriendo = 1;
                    cadena_envio = cargar_archivo(parametros[2]);
                    mandar_mensaje(socket,cadena_envio);
                    transfiriendo = 0;
                }else if(strcmp(parametros[1],"ELIMINAR") == 0){
                    if( remove(parametros[2]) == 0 ) {
                        mandar_mensaje(socket,"SUCCESS");
                        printf("\nexito\n");
                    }
                    else {
                        printf("\nerror\n");
                        mandar_mensaje(socket,"ERROR");
                    }
                }else if(strcmp(parametros[1],"IMPRIMIR_ARCHIVO") == 0){
                    cadena_envio =cargar_archivo(parametros[2]);
                    if( cadena_envio != NULL) {
                        printf("%s",cadena_envio);
                        mandar_mensaje(socket,"SUCCESS");
                    }
                    else {
                        mandar_mensaje(socket,"ERROR");
                    }

                }
                free(parametros);
            }else {
                printf("%s", rbuf);
            }
        }
    } else{
        printf("\nSe a perdido la conexion con el servidor\n");
        return 0;
    }
    return 1;
}


void *hilo_escuchador(void *socket){
    int *socket_pointer = (int *) socket;
    int socket_id = *socket_pointer;


    while (escuchar_servidor(socket)){

    }
    return  NULL;
}



