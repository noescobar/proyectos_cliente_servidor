#include "cliente_headers/hilos_cliente.h"



int main( int argc, char *argv[])
{

    int *socket_id = (int *)malloc(sizeof(int)*1);
    struct sockaddr_in from;
    username = NULL;
    int rc, cc;

    *socket_id = conectar_servidor(&from, argv, argc);
    from_global = from;
    pthread_t thread_id_escuchador;
    pthread_t thread_id_lector;

    pthread_create(&thread_id_lector, NULL, &hilo_lector_teclado, socket_id);
    pthread_create(&thread_id_escuchador, NULL, &hilo_escuchador, socket_id);

    pthread_join(thread_id_escuchador,NULL);

    printf ("\n\nSaliendo...\n\n");

    close(*socket_id);
    exit (0);
}