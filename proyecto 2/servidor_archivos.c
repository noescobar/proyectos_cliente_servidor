#include "servidor_headers/protocolos_servidor_archivos.h"



/*  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
 *                                                                                                                     *
 *  Proyecto                                                                                                           *
 *       Ruleta online implementada con c y postgres (servidor).                                                       *
 *                                                                                                                     *
 *  nota del archivo: Este archivo se construye con el objetivo de utilizar la utleta de forma local para simular el   *
 *                    sin conexion y de esta forma que no hayn problemas.                                              *
 *                                                                                                                     *
 *  Autores:                                                                                                           *
 *      Nelson Orlan Escobar Ceballos                                                                                  *
 *      Luis Alfonso Zuleta                                                                                            *
 *                                                                                                                     *
 *  Requerimientos:                                                                                                    *
 *      Debido  a  que el servidor dependera sobretodo de una base de datos postgresql es importante que se instale lo *
 *      siguiente aparte de obviamente la base de datos postgresql que debera esta funcionando en el puerto 5432, para *
 *      que funcione el servidor debemos tener instalada la libreria libpq-fe.h la cual se consigue tener instalada de *
 *      la siguiente forma:                                                                                            *
 *                                                                                                                     *
 *            Para Ubuntu/Debian/LinuxMint : sudo apt-get install libpq-dev                                            *
 *            Para Fedora/RedHat/Opensuse  : yum install postgresql-devel                                              *
 *                                                                                                                     *
 *  Compilacion:                                                                                                       *
 *      Debido a que este servidor utiliza la libreria libpq-f para hacer la comunicacion y manipulacion de postgres   *
 *      en el codigo c es necesario tener en cuenta el siguiente comando de como se debe compilar el codigo c de este  *
 *      servidor de forma correcta y que no hayan problemas:                                                           *
 *                                                                                                                     *
 *                  gcc -o <nombre_archivo_ejecutable> servidor.c -lpq -I/usr/include/postgresql/  -pthread            *
 *                                                                                                                     *
 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * *
*/


struct args_hilo_cliente{
    int socket_id;
    struct sockaddr_in from;
};

void *hilo_cliente(void *args){
    struct args_hilo_cliente *argumentos = args;
    int socket = argumentos->socket_id;
    struct sockaddr_in from = argumentos->from;
    char *direccion_cliente = (char *) malloc(sizeof(char)*30);
    sprintf(direccion_cliente,"%s:%d",inet_ntoa(from.sin_addr), from.sin_port);
    print_server("GET",direccion_cliente,"CONECTAR","SUCCESS", "SE HA CONECTADO CON EXITO");
    protocolos_server(&socket, from);
    print_server("DELETE",direccion_cliente,"TERMINAR CLIENTE",NULL, "CLIENTE TERMINADO");
    return  NULL;
}



void inicializar_variables_globales(){
    conector_global = establecer_conexion();
    clientes_contectados = (ListaClientes *) malloc(sizeof(ListaClientes)*1);
    clientes_contectados->cabeza = NULL;
    clientes_contectados->cola = NULL;
    clientes_contectados->tamano = 0;
}



int main(int argc, char *argv[]){
    inicializar_variables_globales();
    int sd, psd;
    struct sockaddr_in from;
    int fromlen = 0;
    inicializar_variables_globales();
    limpiar_tablas(conector_global);
    sd = contruir_servidor(from,fromlen, argv, argc);

    pthread_t thread_id_cliente;

    while(1){
        struct sockaddr_in *from_client = (struct sockaddr_in *) malloc(sizeof(struct sockaddr_in));
        psd = accept(sd, (struct sockaddr *) from_client,(socklen_t *) &fromlen);
        struct args_hilo_cliente *argumentos = (struct args_hilo_cliente *)malloc(sizeof(struct args_hilo_cliente));
        argumentos->socket_id = psd;
        argumentos->from = *from_client;
        pthread_create(&thread_id_cliente, NULL, &hilo_cliente, argumentos );


    }
}