#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include <unistd.h>


/*
 * Taller 4:
 *
 * hacer el taller  13 utilizando semaforos.
 * */


void *crear_proceso(void (*funcion)()){
    int *proceso_hijo;
    proceso_hijo = (int*) malloc(sizeof(int));
    *proceso_hijo = fork();
    switch (*proceso_hijo){
            case -1:
                printf("\n Ha sucedido un error y no puede iniciarse el hilo");
                break;
            case 0:
                funcion();
            break;
            default:
                break;
        }
    return NULL;
}

void *hacermultiplicaciones(int *numero_limite, int *frecuencia){
    int *numero_auxiliar1;
    numero_auxiliar1 = (int*)malloc(sizeof(int));
    *numero_auxiliar1 = 1;
    int *numero_auxiliar2;
    numero_auxiliar2 = (int*)malloc(sizeof(int));
    *numero_auxiliar2 = 1;

    while(*numero_auxiliar1 <= *numero_limite) {
        *numero_auxiliar2 = 1;
        while(*numero_auxiliar2 <= 10) {
            sleep((unsigned)*frecuencia);
            printf("\n %d * %d = %d", *numero_auxiliar1, *numero_auxiliar2, *numero_auxiliar1 * *numero_auxiliar2);
            *numero_auxiliar2 = *numero_auxiliar2 + 1;
        }
        *numero_auxiliar1 = *numero_auxiliar1 + 1;
    }

    free(numero_auxiliar1);
    free(numero_auxiliar2);

    return NULL;
}


void *numeros_primos(int *numero_limite, int *frecuencia){
    int *i;
    i = (int*) malloc(sizeof(int)*1);
    int *j;
    j = (int*) malloc(sizeof(int)*1);
    int *raiz;
    raiz = (int*) malloc(sizeof(int)*1);
    int *es_primo;
    es_primo = (int*) malloc(sizeof(int)*1);
    *es_primo = 1;
    for(*i=0; *i<= *numero_limite; *i = *i + 1){
        *raiz =(int)sqrt((double)*numero_limite);
        *es_primo = 1;
        for (*j = 2; *j < *raiz; *j = *j + 1) {
            if(*i % *j == 0){
                *es_primo = 0;
                break;
            }
        }
        sleep((unsigned)*frecuencia);
        if(*es_primo == 1){
            printf("\n Un numero primo es : %d", *i);
        }
    }
    free(i);
    free(j);
    free(es_primo);
    free(raiz);

    return NULL;
}


void proceso_1(){
    int *multiplicaciones;
    multiplicaciones = (int*)malloc(sizeof(int));
    *multiplicaciones = 100;
    int *frecuencia;
    frecuencia = (int*)malloc(sizeof(int));
    *frecuencia = 1;
    hacermultiplicaciones(multiplicaciones, frecuencia);
    free(multiplicaciones);
    free(frecuencia);
}

void proceso_2(){
    int *multiplicaciones;
    multiplicaciones = (int*)malloc(sizeof(int));
    *multiplicaciones = 20;
    int *frecuencia;
    frecuencia = (int*)malloc(sizeof(int));
    *frecuencia = 2;
    numeros_primos(multiplicaciones, frecuencia);
    free(multiplicaciones);
    free(frecuencia);
}


int main() {

    crear_proceso(&proceso_1);
    crear_proceso(&proceso_2);

    return  0;
}
