#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>

#define STDOUT 1
#define SERV_ADDR (IPPORT_RESERVED+1)


int main()
{
    int rval;
    int sock,length,msgsock;
    struct sockaddr_in server;
    struct sockaddr_in client;
    char buf[1024];
    sock=socket(AF_INET, SOCK_STREAM,0);
    if (sock<0)
    {
        perror("No hay socket de escucha");
        exit(1);
    }
    server.sin_family=AF_INET;
    server.sin_addr.s_addr=htonl(INADDR_ANY);
    server.sin_port = htons(SERV_ADDR);
    if (bind(sock,(struct sockaddr *)&server, sizeof(server))<0)
    {
        perror("Direccion no asignada");
        exit(1);
    }
    listen(sock,1);
    while (1)
    {
        /*Estará bloqueado esperando petición de conexión*/
        msgsock = accept(sock,(struct sockaddr *) &client, 0);
        if (msgsock==-1)
            perror("Conexion no aceptada");
        else
            do
            {
                /*Me dispongo a leer datos de la conexión*/
                printf("\n Numero de puerto \t| direccion ip \t|  Nombre del equipo");
                printf("\n %d \t\t|%s\t\t| %s", client.sin_port, inet_ntoa(client.sin_addr),
                       (char *)gethostbyaddr((char *)&client.sin_addr, sizeof(client.sin_addr),AF_INET));
                printf("\n ");
                memset(buf,0,sizeof(buf));
                rval=(int)read(msgsock,buf,1024);


                if (rval<0)
                    perror("Mensaje no leido");
                else
                    write(STDOUT,buf,(size_t)rval);
            }
            while (rval>0);

        printf("\nConexion cerrada\n");
        close(msgsock);
    }
    exit(0);
}